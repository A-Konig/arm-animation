﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/** Přepravka pro pozici trackeru v prostoru */
public class Position
{
    // Pozice
    public Vector3 position;

    // Rotace
    public Vector3 rotation;

    public Position(float x, float y, float z, float rotX, float rotY, float rotZ)
    {
        position = new Vector3(x, y, z);
        rotation = new Vector3(rotX, rotY, rotZ);
    }

}
