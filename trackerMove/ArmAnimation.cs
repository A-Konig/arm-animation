﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

// hodit všechno do startu přehrávat vypočítaný pozice
// udělat si log kdy se co stalo s jakejma hodnotama

// preprocessing - připravit úhly jako kdyby uživatel směřoval do směru z a seděl v (0,0)
//               - lokální z směřuje do směru globálního z

// zahrnout polohu ramene do váhový fce (pokud ho šoupnu vo cm sem tam, bude to lepší?)

// přepočítat diferenci na derivaci
// přepočítat do základních jednotek, všude násobit deltou
// tzn c mm na posun metry

// jak se animuje v unity

// do práce - inverzní kinematika
// že se to teoreticky dá dělat ručně

// ruka se někde nachází, s nějakejma úhlama
// -> ta poloha má nějakou hodnotu cenový funkce
// pokud ruku posuneme v x o 1 stupeň, hodnota cenový funkce se změní -> ten rozdíl do konečnýho vektoru
// a obdobně pro další úhly

public class ArmAnimation : MonoBehaviour
{
    Positioning positioning;
    Config config;

    public bool right;
    public GameObject caps;

    public InputField folder;

    public GameObject rightArmPrefab;
    public GameObject leftArmPrefab;

    //pozice leading objektů
    public Transform chestPosition;
    public Transform cubeArm;
    public Transform cubeForearm;

    //pozice virtuálních trackerů
    public GameObject armTracker;
    public GameObject forearmTracker;

    //ruka
    private GameObject armObj;
    private Transform arm;
    private Transform shoulder;
    private Transform elbow;
    private Transform wrist;

    //řízení aproximace
    private float delta = 5f;
    private float shift = 1f;
    private float shiftAxis = 0.001f;

    //aplikované úhly
    private float[] shoulderAngles = { 0.0f, 0.0f, 0.0f };
    private float[] elbowAngles = { 0.0f, 0.0f, 0.0f };

    //naměřená data
    DataReader reader;
    private Queue<Position> chestData;
    private Queue<Position> forearmData;
    private Queue<Position> armData;

    //výsledná data
    Queue<Vector3> forearmResult;
    Queue<Vector3> shoulderResult;

    private bool starting = true;
    private bool finished = false;

    private void Start()
    {
        if (starting)
        {
            positioning = new Positioning(chestPosition, cubeArm, cubeForearm, armTracker, forearmTracker);
            config = new Config();

            reader = new DataReader(config.inputpath);
            right = config.right;
            starting = false;
        }
        else
        {
            //from update
            while (!starting && chestData.Count > 0) //config.start && 
            {
                positioning.positionArm(chestData, arm, shoulder, elbow, true, right);

                //posune trackery a vedoucí objekty
                positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
                positioning.moveCubes(forearmData, armData, true);

                //došťouchá ruku
                adjustWholeArm();

                //zapíše pozici do souboru
                write();
            }

            finished = true;
            positioning.cubesToZero(arm);
        }
    }

    private void FixedUpdate()
    {
        if (finished)
        {
            positioning.replayMove(shoulderResult, forearmResult, shoulder, elbow);
            positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
        }
    }

    /** Nastaví ruku před pohybem */
    private void firstFrame()
    {
        right = config.right;

        //spawnout ruku
        if (!right)
        {
            armObj = Instantiate(leftArmPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        }
        else
        {
            armObj = Instantiate(rightArmPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        }

        //nastavit kosti
        arm = armObj.transform;
        Transform armature = arm.Find(config.armature);
        shoulder = armature.transform.Find(config.shoulder);
        elbow = shoulder.transform.Find(config.elbow);
        wrist = elbow.transform.Find(config.wrist);

        //načíst data
        reader.setPath(config.inputpath);
        chestData = reader.readFile(config.chestData);
        forearmData = reader.readFile(config.forearmData);
        armData = reader.readFile(config.armData);

        //novou queue pro vytvořená data
        shoulderResult = new Queue<Vector3>();
        forearmResult = new Queue<Vector3>();

        //umístí rameno a vygeneruje náhodnou první polohu ruky
        positioning.positionArm(chestData, arm, shoulder, elbow, false, right);
        positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
        
        positioning.moveCubes(forearmData, armData, false);

        randFirstPos();
    }

    /** Náhodná první pozice ruky */
    private void randFirstPos()
    {
        //stará váha
        float f0 = MathFunc.weightFunc(armTracker.transform, cubeArm, forearmTracker.transform, cubeForearm);
        System.Random r = new System.Random(2546648);

        //zkusit 100 různých poloh
        for (int i = 0; i < 5; i++)
        {
            Vector3 pastArm = shoulder.eulerAngles;
            Vector3 pastForearm = elbow.eulerAngles;

            //nastavit ruku na náhodnou pozici
            elbow.localEulerAngles = new Vector3(r.Next(-180, 180), r.Next(-180, 180), r.Next(-180, 180));
            shoulder.localEulerAngles = new Vector3(r.Next(-180, 180), r.Next(-180, 180), r.Next(-180, 180));
            positioning.moveTrackers(arm, shoulder, elbow, wrist, right);

            adjustWholeArm();

            //nová váha
            float fn = MathFunc.weightFunc(armTracker.transform, cubeArm, forearmTracker.transform, cubeForearm);

            //jestli sem blíž uložit novou váhu, jinak vrátit
            if ((fn - f0) < -0.0001)
            {
                f0 = fn;
            }
            else
            {
                shoulder.eulerAngles = pastArm;
                elbow.eulerAngles = pastForearm;
                positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
            }
        }
    }

    /** Posune v ose axis o jeden stupeň */
    private void shiftOneDegree(string boneName, char axis)
    {
        float[] newElbowAngles = new float[3];
        float[] newShoulderAngles = new float[3];

        if (axis == 'x')
        {
            if (boneName.Equals("elbow"))
            {
                newElbowAngles[0] = (elbowAngles[0] + shift) % 360;
                newElbowAngles[1] = elbowAngles[1];
                newElbowAngles[2] = elbowAngles[2];

                elbow.localEulerAngles = new Vector3(newElbowAngles[0], newElbowAngles[1], newElbowAngles[2]);
            }
            else if (boneName.Equals("shoulder"))
            {
                newShoulderAngles[0] = (shoulderAngles[0] + shift) % 360;
                newShoulderAngles[1] = shoulderAngles[1];
                newShoulderAngles[2] = shoulderAngles[2];

                shoulder.localEulerAngles = new Vector3(newShoulderAngles[0], newShoulderAngles[1], newShoulderAngles[2]);
            }
        }
        else if (axis == 'y')
        {
            if (boneName.Equals("elbow"))
            {
                newElbowAngles[0] = elbowAngles[0];
                newElbowAngles[1] = (elbowAngles[1] + shift) % 360;
                newElbowAngles[2] = elbowAngles[2];

                elbow.localEulerAngles = new Vector3(newElbowAngles[0], newElbowAngles[1], newElbowAngles[2]);
            } else if (boneName.Equals("shoulder"))
            {
                newShoulderAngles[0] = shoulderAngles[0];
                newShoulderAngles[1] = (shoulderAngles[1] + shift) % 360;
                newShoulderAngles[2] = shoulderAngles[2];

                shoulder.localEulerAngles = new Vector3(newShoulderAngles[0], newShoulderAngles[1], newShoulderAngles[2]);
            }
        }
        else if (axis == 'z')
        {
            if (boneName.Equals("elbow"))
            {
                newElbowAngles[0] = elbowAngles[0];
                newElbowAngles[1] = elbowAngles[1];
                newElbowAngles[2] = (elbowAngles[2] + shift) % 360;

                elbow.localEulerAngles = new Vector3(newElbowAngles[0], newElbowAngles[1], newElbowAngles[2]);
            }
            else if (boneName.Equals("shoulder"))
            {
                newShoulderAngles[0] = shoulderAngles[0];
                newShoulderAngles[1] = shoulderAngles[1];
                newShoulderAngles[2] = (shoulderAngles[2] + shift) % 360;

                shoulder.localEulerAngles = new Vector3(newShoulderAngles[0], newShoulderAngles[1], newShoulderAngles[2]);
            }
        }
    }

    /** Posune v ose axis o jeden milimetr */
    private void shiftOnAxis(char axis)
    {
        if (axis == 'x')
        {
            arm.position = new Vector3(arm.position.x + shiftAxis, arm.position.y, arm.position.z);
        }
        else if (axis == 'y')
        {
            arm.position = new Vector3(arm.position.x, arm.position.y + shiftAxis, arm.position.z);
        }
        else if (axis == 'z')
        {
            arm.position = new Vector3(arm.position.x, arm.position.y, arm.position.z + shiftAxis);
        }
    }

    /** Vypočte hodnoty váhové funkce po jednotlivých posunech. Vytvoří váhový vektor. */
    private void weightVector(float[] f)
    {
        // POSITION OF ARM
        Vector3 oldPosArm = arm.position;

        shiftOnAxis('x');
        positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
        f[6] = MathFunc.weightFunc(armTracker.transform, cubeArm, forearmTracker.transform, cubeForearm);

        arm.position = oldPosArm;
        shiftOnAxis('y');
        positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
        f[7] = MathFunc.weightFunc(armTracker.transform, cubeArm, forearmTracker.transform, cubeForearm);

        arm.position = oldPosArm;
        shiftOnAxis('z');
        positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
        f[8] = MathFunc.weightFunc(armTracker.transform, cubeArm, forearmTracker.transform, cubeForearm);

        arm.position = oldPosArm;

        // SHOULDER
        Vector3 oldPosShoulder = shoulder.localEulerAngles;

        shiftOneDegree("shoulder", 'x');
        positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
        f[0] = MathFunc.weightFunc(armTracker.transform, cubeArm, forearmTracker.transform, cubeForearm);

        shoulder.localEulerAngles = oldPosShoulder;
        shiftOneDegree("shoulder", 'y');
        positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
        f[1] = MathFunc.weightFunc(armTracker.transform, cubeArm, forearmTracker.transform, cubeForearm);

        shoulder.localEulerAngles = oldPosShoulder;
        shiftOneDegree("shoulder", 'z');
        positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
        f[2] = MathFunc.weightFunc(armTracker.transform, cubeArm, forearmTracker.transform, cubeForearm);

        shoulder.localEulerAngles = oldPosShoulder;

        // ELBOW
        Vector3 oldPosElbow = elbow.localEulerAngles;

        shiftOneDegree("elbow", 'x');
        positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
        f[3] = MathFunc.weightFunc(armTracker.transform, cubeArm, forearmTracker.transform, cubeForearm);

        elbow.localEulerAngles = oldPosElbow;
        shiftOneDegree("elbow", 'y');
        positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
        f[4] = MathFunc.weightFunc(armTracker.transform, cubeArm, forearmTracker.transform, cubeForearm);

        elbow.localEulerAngles = oldPosElbow;
        shiftOneDegree("elbow", 'z');
        positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
        f[5] = MathFunc.weightFunc(armTracker.transform, cubeArm, forearmTracker.transform, cubeForearm);

        elbow.localEulerAngles = oldPosElbow;
    }

    /** Aproximuje celou ruku */
    private void adjustWholeArm()
    {
        delta = 5;
        float gamma = 0.001f;

        while (delta > 0.0001)
        {
            float[] f = new float[9];
            Vector3 pastArm = shoulder.eulerAngles;
            Vector3 pastForearm = elbow.eulerAngles;
            Vector3 pastShoulder = arm.position;

            //jaká je momentální situace?
            float f0 = MathFunc.weightFunc(armTracker.transform, cubeArm, forearmTracker.transform, cubeForearm);

            //jaká je situace po posunutí v osách
            weightVector(f);

            //odečet
            f = MathFunc.createShift(f0, f);
            
            //posunout o daný vektor
            //vemu pozici a přičtu k ní
            float newXArm = MathFunc.adjust(shoulderAngles[0] + delta * f[0]);
            float newYArm = MathFunc.adjust(shoulderAngles[1] + delta * f[1]);
            float newZArm = MathFunc.adjust(shoulderAngles[2] + delta * f[2]);

            float newXFor = MathFunc.adjust(elbowAngles[0] + delta * f[3]);
            float newYFor = MathFunc.adjust(elbowAngles[1] + delta * f[4]);
            float newZFor = MathFunc.adjust(elbowAngles[2] + delta * f[5]);

            float newXPos = arm.position.x + delta * f[6];
            float newYPos = arm.position.y + delta * f[7];
            float newZPos = arm.position.z + delta * f[8];

            //nová pozice
            arm.position = new Vector3(newXPos, newYPos, newZPos);
            shoulder.localEulerAngles = new Vector3(newXArm, newYArm, newZArm);
            elbow.localEulerAngles = new Vector3(newXFor, newYFor, newZFor);
            positioning.moveTrackers(arm, shoulder, elbow, wrist, right);

            //zlepšili sme situaci?
            float fnew = MathFunc.weightFunc(armTracker.transform, cubeArm, forearmTracker.transform, cubeForearm);

            if ( (fnew - f0) > -0.0001)
            {
                delta /= 2.0f;
                gamma /= 2.0f;
                arm.position = pastShoulder;
                shoulder.eulerAngles = pastArm;
                elbow.eulerAngles = pastForearm;
                positioning.moveTrackers(arm, shoulder, elbow, wrist, right);
            }
            else
            {
                shoulderAngles[0] = newXArm;
                shoulderAngles[1] = newYArm;
                shoulderAngles[2] = newZArm;

                elbowAngles[0] = newXFor;
                elbowAngles[1] = newYFor;
                elbowAngles[2] = newZFor;
            }

        }
    }

    /** Zapíše polohu ruky do souboru */
    private void write()
    {
        System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
        reader.writeToFile(shoulder.eulerAngles.x + "; " + shoulder.eulerAngles.y + "; " + shoulder.eulerAngles.z + "; "
                          + elbow.eulerAngles.x + "; " + elbow.eulerAngles.y + "; " + elbow.eulerAngles.z, config.outputname);

        forearmResult.Enqueue(new Vector3(elbow.eulerAngles.x, elbow.eulerAngles.y, elbow.eulerAngles.z));
        shoulderResult.Enqueue(new Vector3(shoulder.eulerAngles.x, shoulder.eulerAngles.y, shoulder.eulerAngles.z));

    }

    public void setRight()
    {
        config.right = !config.right;
    }

    public void startAnimation()
    {
        finished = false;
        config.inputpath = folder.text;
        string[] p = config.inputpath.Split(new char[] {'/', '\\'}, StringSplitOptions.RemoveEmptyEntries);

        if (p.Length > 0)
        {
            config.outputname = p[p.Length - 1];
        }

        Destroy(armObj);
        firstFrame();

        reader.removeFile(config.outputname);
        positioning.resetChanges();

        config.start = true;
        Start();
    }

}