﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataMovement : MonoBehaviour
{
    public Transform test;
    
    //trackery
    public GameObject armTracker;
    public GameObject forearmTracker;

    //celá ruka
    public Transform armPosition;
    //rameno
    public Transform shoulder;
    //loket
    public Transform elbow;
    //zápěstí
    public Transform wrist;

    //data
    private DataReader reader;
    private Queue<Position> armData;
    private Queue<Position> forearmData;

    private void Start()
    {
        //načte data ze souboru
        reader = new DataReader("Assets/Data/" + "left-short");
        armData = reader.readFile("Assets/Data/" + "left-short/Arm.csv");
        //forearmarmData = reader.readFile(reader.forearmData);
    }

    void Update()
    {
        //umístí ruku do prostoru
        positionArm();
    }

    /** Umístí ruku do prostoru podle pozice naměřené z trackeru na nadloktí */
    private void positionArm()
    {
        //počítat polohu ramene 
        if (armData.Count > 0)
        {
            Vector3 armTrackerPos = armData.Peek().position;
            armPosition.position = armTrackerPos;

            Vector3 direction = (shoulder.position - elbow.position).normalized;
            Vector3 pos = armTrackerPos + direction * Vector3.Distance(shoulder.position, elbow.position) * (0.5f);

            test.position = armTrackerPos;
            armPosition.position = pos;

            armData.Dequeue();
            //moveTrackers();
        }
    }

    /** Vypočte novou polohu trackerů */
    private void moveTrackers()
    {
        //umístit
        armTracker.transform.position = Vector3.MoveTowards(shoulder.position, elbow.position, Vector3.Distance(shoulder.position, elbow.position) / 2f);
        forearmTracker.transform.position = Vector3.MoveTowards(elbow.position, wrist.position, Vector3.Distance(elbow.position, wrist.position) / 2f);

        armTracker.transform.rotation = shoulder.transform.rotation;
        forearmTracker.transform.rotation = elbow.transform.rotation;

        //vystrčit
        Vector3 crossArm = Vector3.Cross(armTracker.transform.position, armTracker.transform.forward) * 100;
        armTracker.transform.position = Vector3.MoveTowards(armTracker.transform.position, crossArm, -Vector3.Distance(shoulder.position, elbow.position) / 6f);

        Vector3 crossForearm = Vector3.Cross(forearmTracker.transform.position, forearmTracker.transform.forward) * 100;
        forearmTracker.transform.position = Vector3.MoveTowards(forearmTracker.transform.position, crossForearm, -Vector3.Distance(elbow.position, wrist.position) / 6f);

    }
}
