﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;

public class DataReader
{
    private string path;

    private StreamWriter file;

    //počet sloupců v txt souboru
    private int columnNumber = 7;

    public DataReader(string path)
    {
        this.path = path;
    }

    /** Vrací data načtená ze souboru */
    public Queue<Position> readFile(string name)
    {
        Queue<Position> res = new Queue<Position>();

        //přečíst všechny řádky
        string[] lines = System.IO.File.ReadAllLines(@path + "/" + name);

        //zpracovat všechny řádky
        for (int i = 0; i < lines.Length; i++)
        {
            //rozdělit na sloupce
            string[] words = lines[i].Split(';'); //nebo ,
            if (words.Length == columnNumber)
            {
                res.Enqueue(new Position(float.Parse(words[1].Trim(), CultureInfo.InvariantCulture), float.Parse(words[2].Trim(), CultureInfo.InvariantCulture), float.Parse(words[3].Trim(), CultureInfo.InvariantCulture),
                float.Parse(words[4].Trim(), CultureInfo.InvariantCulture), float.Parse(words[5].Trim(), CultureInfo.InvariantCulture), float.Parse(words[6].Trim(), CultureInfo.InvariantCulture)));
            }
        }

        return res;
    }

    public void writeToFile(string line, string name)
    {
        file = new StreamWriter(@"Assets/Output/" + name + ".csv", true);
        file.WriteLine(line);
        file.Flush();
        file.Close();
    }
    
    internal void removeFile(string name)
    {
        File.Delete(Path.Combine(@"Assets/Output/", name + ".csv"));
    }

    public void setPath(string path)
    {
        this.path = path;
    }
}
