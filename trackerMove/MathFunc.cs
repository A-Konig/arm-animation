﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathFunc
{

    /** Přetransformuje úhel z intervalu 0 - 360 na interval -180 - 180 */
    public static float adjust(float val)
    {

        float res = val;

        if (val > 180)
        {
            val = val % 360;
            res = -180 + (val - 180);
        }
        else if (val < -180)
        {
            val = val % 360;
            res = 180 + (val + 180);
        }

        return res;
    }

    /** Vzdálenost mezi pozicí trackeru a cube */
    public static float distance(Transform tracker, Transform cube)
    {
        float dist = Vector3.SqrMagnitude(cube.localPosition - tracker.localPosition);

        return dist;
    }

    /** Rozdíl mezi úhly */
    internal static double angleDiff(Transform tracker, Transform cube, char axis)
    {
        Vector3 v = new Vector3(0, 0, 0);
        Vector3 u = new Vector3(0, 0, 0);
        double angle = 0;

        if (axis == 'x')
        {
            u = tracker.transform.right;
            v = cube.transform.right;
        }
        else if (axis == 'y')
        {
            u = tracker.transform.up;
            v = cube.transform.up;
        }
        else if (axis == 'z')
        {
            u = tracker.transform.forward;
            v = cube.transform.forward;
        }

        double d = (u.magnitude * v.magnitude);
        if (d != 0)
        {
            var c = (u.x * v.x + u.y * v.y + u.z * v.z) / d;
            if (c < -1) c = -1;
            if (c > 1) c = 1;
            angle = Math.Acos(c) * Mathf.Rad2Deg;
        }

        return angle;
    }

    /** vypočte hodnotu cenové funkce */
    public static float weightFunc(Transform armTracker, Transform cubeArm, Transform forearmTracker, Transform cubeForearm)
    {
        float f = 0.1f;

        float a = 0.1f;
        float b = 0.1f;
        float c = 0.1f;

        // vzdálenost + a-krát rozdíl úhlů + b-krát rozdíl úhlů + c-krát rozdíl úhlů
        float weightShoulder = (float)(f * MathFunc.distance(armTracker.transform, cubeArm)
                             + a * MathFunc.angleDiff(armTracker, cubeArm, 'x') * MathFunc.angleDiff(armTracker, cubeArm, 'x')
                             + b * MathFunc.angleDiff(armTracker, cubeArm, 'y') * MathFunc.angleDiff(armTracker, cubeArm, 'y')
                             + c * MathFunc.angleDiff(armTracker, cubeArm, 'z') * MathFunc.angleDiff(armTracker, cubeArm, 'z'));
        float weightElbow = (float)(f * MathFunc.distance(forearmTracker.transform, cubeForearm)
                          + a * MathFunc.angleDiff(forearmTracker, cubeForearm, 'x') * MathFunc.angleDiff(forearmTracker, cubeForearm, 'x')
                          + b * MathFunc.angleDiff(forearmTracker, cubeForearm, 'y') * MathFunc.angleDiff(forearmTracker, cubeForearm, 'y')
                          + c * MathFunc.angleDiff(forearmTracker, cubeForearm, 'z') * MathFunc.angleDiff(forearmTracker, cubeForearm, 'z'));

        float res = weightShoulder + weightElbow;

        return res;
    }

    public static float[] createShift(float f0, float[] f)
    {
        // f0 > f -> chcem se posunout

        f[0] = f0 - f[0];
        f[1] = f0 - f[1];
        f[2] = f0 - f[2];
        f[3] = f0 - f[3];
        f[4] = f0 - f[4];
        f[5] = f0 - f[5];
        f[6] = (f0 - f[6]) * 1000f;
        f[7] = (f0 - f[7]) * 1000f;
        f[8] = (f0 - f[8]) * 1000f;

        return f;
    }
}