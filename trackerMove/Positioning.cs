﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// asi bylo lepší když se centrovalo v každym famu na 0

public class Positioning
{
    Transform chestPosition;
    Transform cubeArm;
    Transform cubeForearm;
    GameObject armTracker;
    GameObject forearmTracker;

    private Vector3 startArmPos;
    private Vector3 translate;
    private float rotY;

    public Positioning(Transform chestPosition, Transform cubeArm, Transform cubeForearm,
                       GameObject armTracker, GameObject forearmTracker)
    {
        this.chestPosition = chestPosition;
        this.cubeArm = cubeArm;
        this.cubeForearm = cubeForearm;
        this.armTracker = armTracker;
        this.forearmTracker = forearmTracker;

        translate = new Vector3(float.NaN, -1, -1); //new Vector3(0, 0, 0);
        rotY = float.NaN; //0
        startArmPos = new Vector3(float.NaN, 0, 0);
    }

    public void getFrame(int n, Queue<Position> armData, Queue<Position> forearmData)
    {
        Position[] arm = armData.ToArray();
        Position[] forearm = forearmData.ToArray();

        Vector3 forearmTrackerPos = forearm[n].position;
        Vector3 forearmTrackerRot = forearm[n].rotation;
        cubeForearm.position = forearmTrackerPos;
        cubeForearm.eulerAngles = forearmTrackerRot;

        Vector3 armTrackerPos = arm[n].position;
        Vector3 armTrackerRot = arm[n].rotation;
        cubeArm.position = armTrackerPos;
        cubeArm.eulerAngles = armTrackerRot;
    }

    public void cubesToZero(Transform arm)
    {
        cubeArm.position = new Vector3(0, 0, 0);
        cubeArm.eulerAngles = new Vector3(0, 0, 0);

        cubeForearm.position = new Vector3(0, 0, 0);
        cubeForearm.eulerAngles = new Vector3(0, 0, 0);

        chestPosition.position = new Vector3(0, 0, 0);
        chestPosition.eulerAngles = new Vector3(0, 0, 0);

        arm.position = startArmPos;
    }

    public void replayMove(Queue<Vector3> shoulderRot, Queue<Vector3> forearmRot, Transform shoulder, Transform elbow)
    {
        if (shoulderRot != null  && shoulderRot.Count > 0)
        {
            shoulder.eulerAngles = shoulderRot.Peek();
            elbow.eulerAngles = forearmRot.Peek();

            shoulderRot.Dequeue();
            forearmRot.Dequeue();
        }
    }

    /** Posune vedoucí objekty */
    public void moveCubes(Queue<Position> forearmData, Queue<Position> armData, bool remove)
    {
        if (forearmData.Count > 0)
        {
            Vector3 forearmTrackerPos = forearmData.Peek().position;
            Vector3 forearmTrackerRot = forearmData.Peek().rotation;
            cubeForearm.position = (forearmTrackerPos - translate);
            cubeForearm.eulerAngles = forearmTrackerRot;

            cubeForearm.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), rotY);

            if (remove) forearmData.Dequeue();
        }
        if (armData.Count > 0)
        {
            Vector3 armTrackerPos = armData.Peek().position;
            Vector3 armTrackerRot = armData.Peek().rotation;
            cubeArm.position = (armTrackerPos - translate);
            cubeArm.eulerAngles = armTrackerRot;

            cubeArm.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), rotY);

            if (remove) armData.Dequeue();
        }
    }

    /** Umístí ruku do prostoru podle pozice naměřené z trackeru na nadloktí */
    public void positionArm(Queue<Position> chestData, Transform arm, Transform shoulder, Transform elbow, bool remove, bool right)
    {
        //počítat polohu ramene 
        if (chestData.Count > 0)
        {
            Vector3 chestPos;

            translate = chestData.Peek().position;
            chestPos = new Vector3(0, 0, 0);        //chestData.Peek().position;

            chestPosition.position = chestPos;
            chestPosition.eulerAngles = chestData.Peek().rotation;

            //orientovat hrudník dopředu, poznamenat jeho rotaci
            rotY = (Mathf.Atan2(chestPosition.forward.z, chestPosition.forward.x) / Mathf.PI * 180 - 90);
            chestPosition.transform.Rotate(new Vector3(0, 1, 0), rotY, Space.World);

            arm.position = chestPos;
            arm.rotation = chestPosition.rotation;

            //nahoru
            Vector3 direction = chestPosition.up.normalized;
            Vector3 pos = chestPos + direction * Vector3.Distance(shoulder.position, elbow.position) / 2f; //1.3

            //doleva/doprava
            Vector3 left = new Vector3();
            if (right)
            {
                left = chestPosition.right.normalized;
            }
            else
            {
                left = -chestPosition.right.normalized;
            }
            pos = pos + left * Vector3.Distance(shoulder.position, elbow.position) / 3f;

            //a dozadu
            Vector3 back = -chestPosition.forward.normalized;
            pos = pos + back * Vector3.Distance(shoulder.position, elbow.position) / 5f; //6

            arm.position = pos;
            startArmPos = pos;

            if (remove)
            {
                chestData.Dequeue();
            }
        }
    }

    /** Vypočte novou polohu trackerů */
    public void moveTrackers(Transform arm, Transform shoulder, Transform elbow, Transform wrist, bool right)
    {
        armTracker.transform.rotation = shoulder.transform.rotation;
        forearmTracker.transform.rotation = elbow.transform.rotation;

        //umístit
        armTracker.transform.position = Vector3.MoveTowards(shoulder.position, elbow.position, Vector3.Distance(shoulder.position, elbow.position) / 2f);
        forearmTracker.transform.position = wrist.position;

        //vysunout
        Vector3 crossArm = Vector3.Cross(elbow.position - shoulder.position, armTracker.transform.forward) * 100;
        armTracker.transform.position = Vector3.MoveTowards(armTracker.transform.position, crossArm, Vector3.Distance(shoulder.position, elbow.position) / 6f);

        Vector3 crossForearm = Vector3.Cross(wrist.position - elbow.position, forearmTracker.transform.forward) * 100;
        forearmTracker.transform.position = Vector3.MoveTowards(forearmTracker.transform.position, crossForearm, Vector3.Distance(elbow.position, wrist.position) / 6f);

        armTracker.transform.rotation = shoulder.transform.rotation;
        forearmTracker.transform.rotation = elbow.transform.rotation;

        /*
         modrá od ruky, zelená doprava
         zelená dolu, modrá od ruky, červena doleva
        */

        if (right)
        {
            armTracker.transform.Rotate(new Vector3(-90, 0, 90), Space.Self);
            forearmTracker.transform.Rotate(new Vector3(-90, 0, 90), Space.Self);
        }
        else
        {
            armTracker.transform.Rotate(new Vector3(90, 0, -90), Space.Self);
            forearmTracker.transform.Rotate(new Vector3(90, 0, -90), Space.Self);
        }
    }

    public void resetChanges()
    {
        translate = new Vector3(float.NaN, 0, 0);
        rotY = float.NaN;
        startArmPos = new Vector3(float.NaN, 0, 0);
    }

}
