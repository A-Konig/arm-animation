﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Config
{
    public string inputpath;
    public string outputname;
    public bool right;
    public bool start;

    public string armature = "Armature";
    public string shoulder = "Bone";
    public string elbow = "Bone_001";
    public string wrist = "Bone_002";

    //Názvy souborů
    public string forearmData = "Forearm.csv";
    public string armData = "Arm.csv";
    public string chestData = "Chest.csv";
    public string headData = "Head.csv";

    public Config()
    {
        inputpath = "Assets/Data/rotated/diagonal-left-short-flex";
        outputname = "diagonal-left-short-flex";
        right = false;
        start = false;
    }

}
