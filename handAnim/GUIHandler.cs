﻿using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class GUIHandler : MonoBehaviour
{
    public InputField handAnimation;
    public InputField armAnimation;
    public GameObject leftArm;

    //public GameObject rightArm;

    private AnimationControl animScript;

    public void Start()
    {
        animScript = (AnimationControl)leftArm.GetComponent("AnimationControl");
    }

    public void Replay()
    {
        animScript.Replay();
    }

    public void StartProcessing()
    {
        Debug.Log("Extract");

        // Set path
        if (handAnimation.text.Length != 0)
        {
            animScript.setHandAnimName(handAnimation.text);
        }

        // Start
        animScript.Extract();
    }

    public void Join()
    {
        if ( (armAnimation.text.Length != 0) && (File.Exists(armAnimation.text)) )
        {
            animScript.setArmAnimName(armAnimation.text);
        }

        // Start
        animScript.Join();
    }

    public void SelectArmAnimation()
    {
        string path = EditorUtility.OpenFilePanel("Select folder with data", @"Assets/Data", "");
        armAnimation.text = path;
    }

    public void SetArm()
    {
        animScript.toggleRight();
    }

}