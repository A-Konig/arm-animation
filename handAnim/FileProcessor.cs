﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;

public class FileProcessor
{
    public static void joinAnimations(string handAnimName, string armAnimName, string outputName, int bonesCount)
    {
        // otevřít soubory arm animation a hand animation
        string[] armLines = File.ReadAllLines(@armAnimName);
        int armLinesCount = armLines.GetLength(0);

        string[] handLines = File.ReadAllLines(@"Assets/Output/" + handAnimName + ".csv");
        int handLinesCount = handLines.GetLength(0);
        handLinesCount = handLinesCount / bonesCount;

        Debug.Log("bonesCount=" + bonesCount + " handLinesCount=" + handLinesCount + " armLinesCount=" + armLinesCount);

        FileProcessor.removeFile("Assets/Output/" + outputName);
        //removeFile("Assets/Output/Indices.txt");

        // pro každou řádku v take najít řádky v handAnim
        for (int i = 0; i < armLinesCount; i++)
        {
            int index = (int)Mathf.Round(i * (float)(handLinesCount - 1) / (armLinesCount - 1));

            //zapíšu řádku i z armLines
            FileProcessor.writeToFile(armLines[i], "Assets/Output/" + outputName);

            //zapíšu bonesCount řádek od indexu index z handLines
            //writeToFile(i + " -> " + index + " -> " + (index * bonesCount), "Assets/Output/Indices.txt");

            for (int j = 0; j < bonesCount; j++)
            {
                FileProcessor.writeToFile(handLines[(index * bonesCount) + j], "Assets/Output/" + outputName);
            }

        }

    }

    /** Vrací data načtená ze souboru */
    public static Queue<Position> readAnimation(string filename, int bonesCount)
    {
        Queue<Position> res = new Queue<Position>();

        //přečíst všechny řádky
        string[] lines = File.ReadAllLines(@filename);

        int sets = lines.Length / (bonesCount + 1);

        //zpracovat všechny řádky
        for (int i = 0; i < sets; i++)
        {
            int index = i * (bonesCount + 1);
            string[] words = lines[index].Split(';'); //nebo ,
            res.Enqueue(
                    new Position(float.Parse(words[0].Trim(), CultureInfo.InvariantCulture),
                                 float.Parse(words[1].Trim(), CultureInfo.InvariantCulture),
                                 float.Parse(words[2].Trim(), CultureInfo.InvariantCulture)
                                ));
            res.Enqueue(
                    new Position(float.Parse(words[3].Trim(), CultureInfo.InvariantCulture),
                                 float.Parse(words[4].Trim(), CultureInfo.InvariantCulture),
                                 float.Parse(words[5].Trim(), CultureInfo.InvariantCulture)
                                ));

            //rozdělit na sloupce
            for (int j = 1; j <= bonesCount; j++)
            {
                words = lines[index + j].Split(';'); //nebo ,
                res.Enqueue(
                    new Position(float.Parse(words[0].Trim(), CultureInfo.InvariantCulture),
                                 float.Parse(words[1].Trim(), CultureInfo.InvariantCulture),
                                 float.Parse(words[2].Trim(), CultureInfo.InvariantCulture)
                                ));
            }
        }

        return res;
    }

    public static void removeFile(string path)
    {
        File.Delete(Path.Combine(@path));
    }

    public static void writeToFile(string line, string path)
    {
        StreamWriter file = new StreamWriter(path, true);
        file.WriteLine(line);
        file.Flush();
        file.Close();
    }

}