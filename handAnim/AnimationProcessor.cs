﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationProcessor : MonoBehaviour
{

    public bool AnimationHandling(Animator anim, string handAnimName, Transform armature, bool on)
    {
        if (anim == null)
        {
            return on;
        }

        if (anim.GetCurrentAnimatorStateInfo(0).IsName(handAnimName))
        {
            on = true;
        }
        else if (on)
        {
            on = false;
            Debug.Log("Finished writing");
            return on;
        }

        if (on)
        {
            Transform[] allChildren = armature.GetComponentsInChildren<Transform>();
            foreach (Transform child in allChildren)
            {
                string line = child.localEulerAngles.x + ";" + child.localEulerAngles.y + ";" + child.localEulerAngles.z;
                FileProcessor.writeToFile(line, "Assets/Output/" + handAnimName + ".csv");
            }
        }

        return on;
    }

    public bool ReplayHandling(Animator anim, Queue<Position> queue, Transform armature, Transform forearm, Transform upperarm)
    {
        if (queue != null)
        {

            Position pos = queue.Peek();
            upperarm.eulerAngles = new Vector3(pos.x, pos.y, pos.z);
            queue.Dequeue();

            pos = queue.Peek();
            forearm.eulerAngles = new Vector3(pos.x, pos.y, pos.z);
            queue.Dequeue();

            Transform[] allChildren = armature.GetComponentsInChildren<Transform>();
            foreach (Transform child in allChildren)
            {
                pos = queue.Peek();
                child.localEulerAngles = new Vector3(pos.x, pos.y, pos.z);
                queue.Dequeue();
            }

            Debug.Log("in queue: " + queue.Count);

            if (queue.Count == 0)
            {
                anim.enabled = true;
                return false;
            }

        }

        return true;
    }

}
