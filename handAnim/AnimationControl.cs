﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;

public class AnimationControl : MonoBehaviour
{
    private bool on;
    private bool replay;
    private bool join;
    private bool start;
    private bool right;

    private string outputName;
    private int bonesCount;
    private Transform armature;
    private StreamWriter file;
    private Queue<Position> queue;
    private GameObject arm;
    private Animator anim;
    private Transform upperArm;
    private Transform forearm;

    private AnimationProcessor ap;

    public RuntimeAnimatorController animContL;
    public RuntimeAnimatorController animContR;
    public string armAnimName;
    public string handAnimName;
    public GameObject left_arm;
    public GameObject right_arm;

    // Use this for initialization
    void Start()
    {
        string[] names = armAnimName.Split(new char[] { '/', '\\' }, StringSplitOptions.RemoveEmptyEntries);
        names = names[names.Length - 1].Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
        outputName = names[0] + "_result.csv"; 

        System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

        ap = (AnimationProcessor)this.GetComponent("AnimationProcessor");

        on = false;
        replay = false;
        join = false;
        right = false;
        start = false;
        queue = null;
    }

    internal void toggleRight()
    {
        right = !right;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (join)
        {
            Debug.Log("Joining animations");
            //FileProcessor.joinAnimations(handAnimName, armAnimName, outputName, bonesCount);
            joinAnimations();
            join = false;
        }
        if (start)
        {
            Debug.Log("Writing to file");
            StartAnimation();
            start = false;
        }

        on = ap.AnimationHandling(anim, handAnimName, armature, on);
        if (replay)
        {
            replay = ap.ReplayHandling(anim, queue, armature, forearm, upperArm);
        }
    }

    internal void Extract()
    {
        start = true;
    }

    internal void Join()
    {
        join = true;
    }

    private void StartAnimation()
    {
        InstantiateArm();
        FileProcessor.removeFile("Assets/Output/" + handAnimName + ".csv");
        anim.Play(handAnimName);
    }

    public void Replay()
    {
        Debug.Log("Reading from file");
        replay = true;
        queue = FileProcessor.readAnimation("Assets/Output/" + outputName, bonesCount);
        anim.enabled = false;
    }

    private void joinAnimations()
    {
        Debug.Log("Joining: " + armAnimName + " + " + handAnimName);

        // otevřít soubory arm animation a hand animation
        string[] armLines = File.ReadAllLines(@armAnimName);
        int armLinesCount = armLines.GetLength(0);

        string[] handLines = File.ReadAllLines(@"Assets/Output/" + handAnimName + ".csv");
        int handLinesCount = handLines.GetLength(0);
        handLinesCount = handLinesCount / bonesCount;

        Debug.Log("bonesCount=" + bonesCount + " handLinesCount=" + handLinesCount + " armLinesCount=" + armLinesCount);

        FileProcessor.removeFile("Assets/Output/" + outputName);
        //removeFile("Assets/Output/Indices.txt");

        // pro každou řádku v take najít řádky v handAnim
        for (int i = 0; i < armLinesCount; i++)
        {
            int index = (int) Mathf.Round(i * (float)(handLinesCount-1) / (armLinesCount-1));

            //zapíšu řádku i z armLines
            FileProcessor.writeToFile(armLines[i], "Assets/Output/" + outputName);

            //zapíšu bonesCount řádek od indexu index z handLines
            //writeToFile(i + " -> " + index + " -> " + (index * bonesCount), "Assets/Output/Indices.txt");

            for (int j = 0; j < bonesCount; j++)
            {
                FileProcessor.writeToFile(handLines[ (index * bonesCount) + j], "Assets/Output/" + outputName);
            }

        }

    }

    /*
    private void AnimationHandling()
    {
        if (anim == null)
        {
            return;
        }

        if (anim.GetCurrentAnimatorStateInfo(0).IsName(handAnimName))
        {
            on = true;
        }
        else if (on)
        {
            on = false;
            Debug.Log("Finished writing");
        }

        if (on)
        {
            Transform[] allChildren = armature.GetComponentsInChildren<Transform>();
            foreach (Transform child in allChildren)
            {
                string line = child.localEulerAngles.x + ";" + child.localEulerAngles.y + ";" + child.localEulerAngles.z;
                FileProcessor.writeToFile(line, "Assets/Output/" + handAnimName + ".csv");
            }
        }
    }

    private void ReplayHandling(Queue<Position> queue)
    {
        if ( (replay) && (queue != null) )
        {

            // nejdřív vzít první dvě a nastavit je paži
            Position pos = queue.Peek();
            upperArm.eulerAngles = new Vector3(pos.x, pos.y, pos.z);
            queue.Dequeue();

            pos = queue.Peek();
            forearm.eulerAngles = new Vector3(pos.x, pos.y, pos.z);
            queue.Dequeue();

            Transform[] allChildren = armature.GetComponentsInChildren<Transform>();
            foreach (Transform child in allChildren)
            {
                pos = queue.Peek();
                child.localEulerAngles = new Vector3(pos.x, pos.y, pos.z);
                queue.Dequeue();
            }

            if (queue.Count == 0)
            {
                anim.enabled = true;
                replay = false;
            }

            Debug.Log("in queue: " + queue.Count);
        }
    }

     */

    public void setHandAnimName(string handAnimName)
    {
        this.handAnimName = handAnimName;
    }

    public void setArmAnimName(string armAnimName)
    {
        this.armAnimName = armAnimName;

        string[] names = armAnimName.Split(new char[] { '/', '\\' }, StringSplitOptions.RemoveEmptyEntries);
        names = names[names.Length - 1].Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
        outputName = names[0] + "_result.csv";
    }

    private void InstantiateArm()
    {
        if (arm != null)
        {
            Destroy(arm);
        }

        if (right)
        {
            arm = Instantiate(right_arm, new Vector3(0, 0, 0), Quaternion.identity);
            anim = arm.GetComponent<Animator>();
            anim.runtimeAnimatorController = animContR;
        }
        else
        {
            arm = Instantiate(left_arm, new Vector3(0, 0, 0), Quaternion.identity);
            anim = arm.GetComponent<Animator>();
            anim.runtimeAnimatorController = animContL;
        }

        armature = arm.transform.Find("Armature/Bone/Bone_001/Bone_002");

        bonesCount = 0;
        Transform[] allChildren = armature.GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            bonesCount++;
        }

        upperArm = arm.transform.Find("Armature/Bone");
        forearm = arm.transform.Find("Armature/Bone/Bone_001");

    }

}
