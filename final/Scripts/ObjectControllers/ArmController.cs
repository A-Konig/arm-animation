﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provides methods controlling the position of arm in space and the rotation of its bones
/// </summary>
public class ArmController : MonoBehaviour
{
    /// <summary> Left arm prefab </summary>
    public GameObject left_arm;
    /// <summary> Right arm prefab </summary>
    public GameObject right_arm;

    /// <summary> Arm game object </summary>
    internal GameObject arm;
    /// <summary> Armature </summary>
    internal Transform armature;
    /// <summary> Upperarm bone </summary>
    internal Transform upperarm;
    /// <summary> Forearm bone </summary>
    internal Transform forearm;
    /// <summary> First hand bone </summary>
    internal Transform hand;

    /// <summary> Is the arm right </summary>
    internal bool right;

    /// <summary> One step in position </summary>
    private readonly float shiftAxis = 0.001f;
    /// <summary> One step in rotation </summary>
    private readonly float shift = 1f;

    /// <summary> Setter for arm gameobject </summary>
    internal GameObject Arm
    {
        set
        {
            arm = value;
            hand = arm.transform.Find(Config.handBone);
            upperarm = arm.transform.Find(Config.upperarmBone);
            forearm = arm.transform.Find(Config.forearmBone);
            armature = arm.transform.Find(Config.armature);
        }
    }

    /// <summary>
    /// Instantiates an arm
    /// </summary>
    /// <param name="right"> Should right arm be instantiated </param>
    internal void InstantiateArm(bool right)
    {
        this.right = right;

        if (arm != null)
            Destroy(arm);

        if (right)
            Arm = Instantiate(right_arm, new Vector3(0, 0, 0), Quaternion.identity);
        else
            Arm = Instantiate(left_arm, new Vector3(0, 0, 0), Quaternion.identity);
    }

    /// <summary>
    /// Positions arm in space relative to chest
    /// Arm is positioned 1/2 lenght of upperarm bone up, 1/3 length of upperarm bone left/right, 1/5 length of upperarm bone back
    /// </summary>
    /// <param name="chestData"> Chest data </param>
    /// <param name="trackers"> Tracker controller </param>
    /// <param name="remove"> Should the position from chestData be removed </param>
    internal void PositionArm(Queue<Position> chestData, TrackerController trackers, bool remove)
    {
        if (chestData.Count > 0)
        {
            Vector3 chestPos = new Vector3(0, 0, 0);
            float bonedistance = Vector3.Distance(upperarm.transform.position, forearm.transform.position);

            arm.transform.position = chestPos;
            arm.transform.rotation = trackers.leadingChest.transform.rotation;

            // up
            Vector3 direction = trackers.leadingChest.transform.up.normalized;
            Vector3 pos = chestPos + direction * bonedistance / 2f; 

            // right / left
            Vector3 left = new Vector3();
            if (right)
                left = trackers.leadingChest.transform.right.normalized;
            else
                left = -trackers.leadingChest.transform.right.normalized;
            pos = pos + left * bonedistance / 3f;

            // back
            Vector3 back = -trackers.leadingChest.transform.forward.normalized;
            pos = pos + back * bonedistance / 5f;

            arm.transform.position = pos;

            if (remove)
                chestData.Dequeue();
        }
    }

    /// <summary>
    /// Stop displaying arm
    /// </summary>
    internal void Stop()
    {
        Destroy(arm);
    }

    /// <summary>
    /// Moves arm's shoulder position one step on an axis
    /// </summary>
    /// <param name="axis"> Name of the axis to be altered
    ///     'x' - x axis
    ///     'y' - y axis
    ///     'z' - z axis
    /// </param>
    internal void ShiftOnAxis(char axis)
    {
        if (axis == 'x')
            arm.transform.position = new Vector3(arm.transform.position.x + shiftAxis, arm.transform.position.y, arm.transform.position.z);
        else if (axis == 'y')
            arm.transform.position = new Vector3(arm.transform.position.x, arm.transform.position.y + shiftAxis, arm.transform.position.z);
        else if (axis == 'z')
            arm.transform.position = new Vector3(arm.transform.position.x, arm.transform.position.y, arm.transform.position.z + shiftAxis);
    }

    /// <summary>
    /// Roates bone around axis by one step
    /// </summary>
    /// <param name="boneName"> Name of bone to alter the values of
    ///     "forearm" - forearm bone
    ///     "upperarm" - upperarm bone
    /// </param>
    /// <param name="axis"> Name of the axis to be altered
    ///     'x' - x axis
    ///     'y' - y axis
    ///     'z' - z axis
    /// </param>
    internal void ShiftOneDegree(string boneName, char axis, float[] angles)
    {
        float[] newAngles = new float[3];
        newAngles[0] = angles[0];
        newAngles[1] = angles[1];
        newAngles[2] = angles[2];

        if (axis == 'x')
            newAngles[0] = (angles[0] + shift) % 360;
        else if (axis == 'y')
            newAngles[1] = (angles[1] + shift) % 360;
        else if (axis == 'z')
            newAngles[2] = (angles[2] + shift) % 360;

        if (boneName.Equals("forearm"))
            forearm.localEulerAngles = new Vector3(newAngles[0], newAngles[1], newAngles[2]);
        else if (boneName.Equals("upperarm"))
            upperarm.localEulerAngles = new Vector3(newAngles[0], newAngles[1], newAngles[2]);
    }

    /// <summary>
    /// Moves bones in hand according to bleder animation data in tails
    /// </summary>
    /// <param name="tails"> Contains the position of the tail of bones and the orientation of their z axis </param>
    internal void MoveHand(Queue<Position> tails)
    {
        Transform[] allChildren = hand.GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            Vector3 tail = MathFunc.BlendToUnity(tails.Peek().position);
            Vector3 zAxis = MathFunc.BlendToUnity(tails.Peek().rotation);

            tail = armature.localToWorldMatrix.MultiplyPoint(tail);
            zAxis = armature.localToWorldMatrix.MultiplyVector(zAxis);

            int rot = -90;
            int x = 1;
            if (right)
            {
                rot = 90;
                x = -1;
            }

            child.LookAt(tail, child.transform.up);
            child.Rotate(new Vector3(0, rot, 0));

            zAxis = child.InverseTransformVector(zAxis);
            float rotX = Mathf.Atan2(zAxis.y, zAxis.z) / Mathf.PI * 180;
            child.Rotate(new Vector3(x, 0, 0), rotX, Space.Self);

            tails.Dequeue();
        }
    }
}