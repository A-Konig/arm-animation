﻿using System.Globalization;
using TMPro;
using UnityEngine;

/// <summary>
/// Class controlling the UI of the application
/// </summary>
public class UIHandler : MonoBehaviour
{
    /// <summary> Input field for hand animation file </summary>
    public TMP_InputField handInput;
    /// <summary> Input field for arm animation file </summary>
    public TMP_InputField armInput;
    /// <summary> Input field for final animation file </summary>
    public TMP_InputField replayInput;
    /// <summary> Input field for data folder </summary>
    public TMP_InputField dataInput;

    /// <summary> Camera </summary>
    public GameObject cameraObj;
    /// <summary> Shoulder stand-in </summary>
    public GameObject armShoulder;
    /// <summary> Tracker parent object </summary>
    public GameObject trackers;

    /// <summary> State line controller </summary>
    public StateLine stateLine;
    
    /// <summary> Join controller </summary>
    public JoinController joinController;
    /// <summary> Replay controller </summary>
    public ReplayController replayController;
    /// <summary> Generate controller </summary>
    public GenerateController generateController;

    /// <summary> Is right arm selected </summary>
    private bool right;

    /// <summary>
    /// Performed only once at the start of the application
    /// Sets up other controller objects
    /// </summary>
    public void Start()
    {
        Screen.SetResolution((int)(Screen.currentResolution.width / 1.5f), (int)(Screen.currentResolution.height / 1.5f ), false);
        System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
        right = false;

        Config.OutputFolder = "";
        Config.InputFolder = "";
    }

    /// <summary>
    /// Performed every frame
    /// Listens input, reacts on:
    ///     - ctrl+Q - to stop any ongoing action
    ///     - replay flag from GenerateController
    /// </summary>
    public void Update()
    {
        // Ctrl + Q 
        if (Input.GetKeyDown(KeyCode.Q) && Input.GetKey(KeyCode.LeftControl))
        {
            // stop generating
            if (generateController.generate)
            {
                generateController.Stop();
                stateLine.Stop();
                stateLine.SetText("Generování animace přerušeno");
            }

            // stop replaying animation
            if (replayController.replay)
            {
                replayController.Stop();
                stateLine.Stop();
                stateLine.SetText("Přehrávání přerušeno");
            }

            // stop joining
            if (joinController.joining)
            {
                joinController.Stop();
                stateLine.Stop();
                stateLine.SetText("Spojování animací přerušeno");
            }
        }

        // generated new arm file -> automatic replay
        if (generateController.toReplay)
        {
            generateController.toReplay = false;
            stateLine.Stop();
            stateLine.SetText($"Generování animace {FileManip.GetName(Config.OutputFile)} dokončeno");
            bool res = replayController.StartReplay(Config.OutputFile);
        }
    }

    /// <summary>
    /// Replay animation
    /// </summary>
    public void Replay()
    {
        Config.ReplayPath = replayInput.text;

        try
        {
            bool res = replayController.StartReplay(Config.ReplayPath);

            if (!res)
            {
                stateLine.SetText("Přehrávání neúspěšné.");
            }
            else
            {
                stateLine.SetText("Přehrávání pohybu " + FileManip.GetName(Config.ReplayPath));
                stateLine.Replaying = true;
            }
        }
        catch
        {
            replayController.Stop();
            stateLine.SetText("Přehrávání neúspěšné. Není soubor otevřen v jiné aplikaci?");
        }
    }

    /// <summary>
    /// Join two animation files
    /// </summary>
    public void Join()
    {
        Config.ArmAnimationFile = armInput.text.Trim();
        Config.HandAnimationFile = handInput.text.Trim();
        Config.OutputFile = DataWriter.CreateFinalFile(Config.ArmAnimationFile, Config.HandAnimationFile);
        
        try
        {
            int loadedLength = -1;
            if (FileManip.Exists(Config.HandAnimationFile))
                loadedLength = (DataReader.ReadAllLines(Config.HandAnimationFile)).Length;

            if (loadedLength == -1)
            {
                stateLine.SetText("Špatný vstupní soubor");
                return;
            }

            stateLine.SetText("Slučování " + FileManip.GetName(Config.ArmAnimationFile, true) + " " + FileManip.GetName(Config.HandAnimationFile, true));

            stateLine.DisplayPercentageScreen(loadedLength);

            bool res = joinController.StartJoin(Config.ArmAnimationFile, Config.HandAnimationFile, Config.OutputFile);
            if (!res)
            {
                stateLine.Stop();
                stateLine.SetText("Slučování neúspěšné.");
            }
            else
                stateLine.Joining = true;
        }
        catch
        {
            joinController.Stop();
            stateLine.Stop();
            stateLine.SetText("Slučování neúspěšné. Není některý soubor otevřen v jiné aplikaci?");
        }
    }

    /// <summary>
    /// Generate new arm animation file
    /// </summary>
    public void Generate()
    {
        Config.DataFolderPath = dataInput.text;
        Config.OutputFile = DataWriter.CreateAnimationFile(Config.DataFolderPath);

        try
        {
            int loadedLength = -1;
            if (FileManip.Exists(Config.DataFolderPath + "/" + Config.chestData))
                loadedLength = (DataReader.ReadAllLines(Config.DataFolderPath + "/" + Config.chestData)).Length;

            if (loadedLength == -1)
            {
                stateLine.SetText("Špatná vstupní složka");
                return;
            }

            stateLine.SetText("Generování nové animace " + FileManip.GetName(Config.OutputFile));

            stateLine.DisplayPercentageScreen(loadedLength);

            bool res = generateController.StartGeneration();
            if (!res)
            {
                stateLine.Stop();
                stateLine.SetText("Generování neúspěšné.");
            }
            else
                stateLine.Generating = true;
        } catch
        {
            generateController.Stop();
            stateLine.Stop();
            stateLine.SetText("Generování neúspěšné. Není některý soubor otevřen v jiné aplikaci?");
        }
    }

    /// <summary>
    /// Toggle left/right arm
    /// </summary>
    public void ToggleRight()
    {
        right = !right;
        Config.right = right;
    }

}