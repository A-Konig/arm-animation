﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains tracker objects (virtual trackers and leading objects) and provides methods manipulating with those objects
/// Virtual trackers represent the trackers tied to the virtual arm, leading objects represent the data
/// </summary>
public class TrackerController : MonoBehaviour
{
    /// <summary> Prefab of a tracker object </summary>
    public GameObject tracker;

    /// <summary> Leading upperarm tracker </summary>
    internal GameObject leadingUpper;
    /// <summary> Leading forearm tracker </summary>
    internal GameObject leadingFor;
    /// <summary> Leading chest tracker </summary>
    internal GameObject leadingChest;

    /// <summary> Virtual upperarm tracker </summary>
    internal GameObject virtualUpper;
    /// <summary> Virtual forearm tracker </summary>
    internal GameObject virtualFor;

    /// <summary> Unedited position of chest in data </summary>
    private Vector3 translate;
    /// <summary> Rotation of chest from yz plane in unedited data </summary>
    private float rotY;

    /// <summary>
    /// Instantates virtual trackers
    /// </summary>
    internal void InstantiateTrackers()
    {
        if (virtualUpper != null)
        {
            Destroy(virtualUpper);
            Destroy(virtualFor);
            Destroy(leadingUpper);
            Destroy(leadingFor);
            Destroy(leadingChest);
        }

        virtualUpper = Instantiate(tracker, new Vector3(0, 0, 0), Quaternion.identity, this.transform);
        virtualFor = Instantiate(tracker, new Vector3(0, 0, 0), Quaternion.identity, this.transform);

        leadingChest = Instantiate(tracker, new Vector3(0, 0, 0), Quaternion.identity, this.transform);
        leadingUpper = Instantiate(tracker, new Vector3(0, 0, 0), Quaternion.identity, this.transform);
        leadingFor = Instantiate(tracker, new Vector3(0, 0, 0), Quaternion.identity, this.transform);

        translate = new Vector3(float.NaN, -1, -1);
        rotY = float.NaN;
    }

    /// <summary>
    /// Calls method for calculating the value of the optimalisation function above current configuration of virtual trackers
    /// </summary>
    /// <returns> Value of currrent configuration </returns>
    internal float CalculateObjFunction()
    {
        float w = MathFunc.CalculateObjFunc(virtualUpper.transform, leadingUpper.transform, virtualFor.transform, leadingFor.transform);
        return w;
    }

    /// <summary>
    /// Moves leading objects to another data frame
    /// </summary>
    /// <param name="forData"> Forearm tracker data </param>
    /// <param name="upperData"> Upperarm tracker data </param>
    /// <param name="remove"> Should the frame be removed from data </param>
    internal void MoveLeading(Queue<Position> forData, Queue<Position> upperData, bool remove = true)
    {
        if ((forData.Count > 0) && (upperData.Count > 0))
        {
            Vector3 forearmTrackerPos = forData.Peek().position;
            Vector3 forearmTrackerRot = forData.Peek().rotation;
            leadingFor.transform.position = (forearmTrackerPos - translate);
            leadingFor.transform.eulerAngles = forearmTrackerRot;

            leadingFor.transform.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), rotY);

            Vector3 armTrackerPos = upperData.Peek().position;
            Vector3 armTrackerRot = upperData.Peek().rotation;
            leadingUpper.transform.position = (armTrackerPos - translate);
            leadingUpper.transform.eulerAngles = armTrackerRot;

            leadingUpper.transform.RotateAround(new Vector3(0, 0, 0), new Vector3(0, 1, 0), rotY);

            if (remove)
            {
                forData.Dequeue();
                upperData.Dequeue();
            }
        }
    }

    /// <summary>
    /// Positions leading object representing chest tracker in space, 
    /// the data is always edited so it is located at (0, 0, 0) facing forwards
    /// Calculates attributes rotY and translate
    /// </summary>
    /// <param name="chestData"> Chest tracker data </param>
    internal void PositionChest(Queue<Position> chestData)
    {
        translate = chestData.Peek().position;

        // to zero
        leadingChest.transform.position = new Vector3(0, 0, 0);
        leadingChest.transform.eulerAngles = chestData.Peek().rotation;

        // forwards
        rotY = Mathf.Atan2(leadingChest.transform.forward.z, leadingChest.transform.forward.x) / Mathf.PI * 180 - 90;
        leadingChest.transform.transform.Rotate(new Vector3(0, 1, 0), rotY, Space.World);
    }

    /// <summary>
    /// Positions virtual trackers in correspondence to the virtual arm
    /// Upperarm tracker poitioned to 1/2 of upperarm bone length
    /// Forearm tracker positioned to 1/15 of forearm bone length
    /// Both moved 1/6 of coresponding bone lenght so they are positioned atop of the virual skin
    /// </summary>
    /// <param name="arm"> Arm controller </param>
    internal void MoveVirtual(ArmController arm)
    {
        virtualUpper.transform.rotation = arm.upperarm.rotation;
        virtualFor.transform.rotation = arm.forearm.rotation;

        // bone lengths
        float upperBoneLen = Vector3.Distance(arm.upperarm.position, arm.forearm.position);
        float forBoneLen = Vector3.Distance(arm.forearm.position, arm.hand.position);

        // position
        virtualUpper.transform.position = Vector3.MoveTowards(arm.upperarm.position, arm.forearm.position, upperBoneLen / 2f);
        virtualFor.transform.position = Vector3.MoveTowards(arm.hand.position, arm.forearm.position, forBoneLen / 15f);

        // move to sit atop of the arm "skin"
        Vector3 crossArm = Vector3.Cross(arm.forearm.position - arm.upperarm.position, virtualUpper.transform.forward) * 100;
        virtualUpper.transform.position = Vector3.MoveTowards(virtualUpper.transform.position, crossArm, upperBoneLen / 6f);

        Vector3 crossForearm = Vector3.Cross(arm.hand.position - arm.forearm.position, virtualFor.transform.forward) * 100;
        virtualFor.transform.position = Vector3.MoveTowards(virtualFor.transform.position, crossForearm, forBoneLen / 6f);

        // adjust rotation
        virtualUpper.transform.rotation = arm.upperarm.transform.rotation;
        virtualFor.transform.rotation = arm.forearm.transform.rotation;

        // rotate so orientations are the same as real life trackers
        if (arm.right)
        {
            virtualUpper.transform.Rotate(new Vector3(-90, 0, 90), Space.Self);
            virtualFor.transform.Rotate(new Vector3(-90, 0, 90), Space.Self);
        }
        else
        {
            virtualUpper.transform.Rotate(new Vector3(90, 0, -90), Space.Self);
            virtualFor.transform.Rotate(new Vector3(90, 0, -90), Space.Self);
        }
    }

    /// <summary>
    /// Stop visualisation of trackers
    /// </summary>
    internal void Stop()
    {
        Destroy(leadingUpper);
        Destroy(leadingFor);
        Destroy(leadingChest);

        Destroy(virtualUpper);
        Destroy(virtualFor);
    }
}