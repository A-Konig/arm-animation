﻿using TMPro;
using UnityEngine;

/// <summary>
/// Class controlling the display of output messages on screen
/// </summary>
public class StateLine : MonoBehaviour
{
    /// <summary> Array with text fields to write output into </summary>
    public TMP_Text[] state;

    /// <summary> Instance of the progress screen </summary>
    public GameObject percScreen;
    /// <summary> Number of lines to process in input data </summary>
    private int loadedLenght;

    /// <summary> Replay controller </summary>
    public ReplayController replay;
    /// <summary> Generate controller </summary>
    public GenerateController generate;
    /// <summary> Join controller </summary>
    public JoinController join;
    /// <summary> Camera controller </summary>
    public CameraController cameraCont;

    /// <summary> Is currently replaying </summary>
    internal bool Replaying { get; set; }
    /// <summary> Is currently generating </summary>
    internal bool Generating { get; set; }
    /// <summary> Is currently joining animations </summary>
    internal bool Joining { get; set; }

    /// <summary>
    /// Executes each frame
    /// Checks if any ongoing action is finished, if yes writes corresponding output
    /// </summary>
    private void Update()
    {
        // replaying
        if ( (Replaying) && (!replay.replay) )
        {
            SetText("Přehrávání animace dokončeno");
            Replaying = false;
        }

        // generating
        if ( Generating )
        {
            // new percentage
            double actualLength = generate.animCont.chestData.Count;
            int val = (int)(((loadedLenght - actualLength) / (double)loadedLenght) * 100);
            SetPercentage(val);
        }

        // joining
        if (Joining)
        {
            // new percentage
            if (join.joining)
            {
                double actualLength = join.tails.Count;
                int val = (int)(((loadedLenght - actualLength) / (double)loadedLenght) * 100);
                SetPercentage(val);
            }
            // finished
            else
            {
                SetText($"Spojování animace {FileManip.GetName(Config.OutputFile)} dokončeno");
                Stop();
            }
        }

    }

    /// <summary>
    /// Stop blocking UI
    /// </summary>
    internal void Stop()
    {
        percScreen.SetActive(false);
        if (cameraCont != null)
            cameraCont.ToggleBlock(false);

        if (Generating) Generating = false;
        if (Replaying) Replaying = false;
        if (Joining) Joining = false;
    }

    /// <summary>
    /// Writes new text into textfields, all outputs are moved down on the screen.
    /// </summary>
    /// <param name="text"> New text to display </param>
    internal void SetText(string text)
    {
        state[3].text = state[2].text;
        state[2].text = state[1].text;
        state[1].text = state[0].text;
        state[0].text = text;
    }

    /// <summary>
    /// Sets the displayed percentage on progress screen
    /// </summary>
    /// <param name="val"> New percentage </param>
    internal void SetPercentage(double val)
    {
        TMP_Text child = percScreen.transform.Find("PercentageTXT").gameObject.GetComponent<TMP_Text>();
        child.SetText($"{val.ToString()}%");
    }

    /// <summary>
    /// Enables the black screen blocking the whole window displaying the percentage progress of an ongoing action
    /// </summary>
    /// <param name="loadedLenght"> Number of lines to process in input data </param>
    internal void DisplayPercentageScreen(int loadedLenght)
    {
        this.loadedLenght = loadedLenght;

        percScreen.SetActive(true);
        cameraCont.ToggleBlock(true);
    }
}