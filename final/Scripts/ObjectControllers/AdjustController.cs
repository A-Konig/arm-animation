﻿using System;
using UnityEngine;

/// <summary>
/// Controller for adjusting the angles of bones during the generation of new arm animation
/// Used for calculating new positions while generating an animation
/// </summary>
public class AdjustController : MonoBehaviour
{
    /// <summary> Delta </summary>
    private float delta = 5f;
    /// <summary> Border value of delta </summary>
    private readonly float epsilon = 0.0001f;

    /// <summary> Last accepted rotation angles of upperarm bone </summary>
    private float[] upperAngles = { 0.0f, 0.0f, 0.0f };
    /// <summary> Last accepted rotation angles of forearm bone </summary>
    private float[] forAngles = { 0.0f, 0.0f, 0.0f };
    /// <summary> Last accepted position of arm </summary>
    Vector3 pastShoulder;

    /// <summary> New unevaluated angles of upperarm bone </summary>
    private float[] newUpper = new float[3];
    /// <summary> New unevaluated angles of forearm bone </summary>
    private float[] newFor = new float[3];

    /// <summary> Gradient </summary>
    float[] f;
    /// <summary> Value of last accepted position </summary>
    float f0;

    /// <summary>
    /// Reset stored last accepted angles to current angles on arm object
    /// Called before the start of generating animation frames
    /// </summary>
    /// <param name="arm"> Arm controller </param>
    internal void StartAngles(ArmController arm)
    {
        upperAngles[0] = arm.upperarm.rotation.eulerAngles.x;
        upperAngles[1] = arm.upperarm.rotation.eulerAngles.y;
        upperAngles[2] = arm.upperarm.rotation.eulerAngles.z;

        forAngles[0] = arm.forearm.rotation.eulerAngles.x;
        forAngles[1] = arm.forearm.rotation.eulerAngles.y;
        forAngles[2] = arm.forearm.rotation.eulerAngles.z;
    }

    /// <summary>
    /// Sets random first position of arm, the goal is to generate the closest starting position for further improvement
    /// Randomly sets all angles of upperarm and forearm bone, and tries to optimalize the position
    /// </summary>
    /// <param name="arm"> Arm controller </param>
    /// <param name="trackers"> Tracker controller </param>
    internal void RandFirstPos(ArmController arm, TrackerController trackers)
    {
        float f0 = trackers.CalculateObjFunction();
        System.Random r = new System.Random(Config.seed);

        // try new positions
        for (int i = 0; i < Config.pertTimes; i++)
        {
            Vector3 pastArm = arm.upperarm.eulerAngles;
            Vector3 pastForearm = arm.forearm.eulerAngles;
            Vector3 pastPosition = arm.arm.transform.position;

            // random position
            arm.forearm.localEulerAngles = new Vector3(r.Next(-180, 180), r.Next(-180, 180), r.Next(-180, 180));
            arm.upperarm.localEulerAngles = new Vector3(r.Next(-180, 180), r.Next(-180, 180), r.Next(-180, 180));
            trackers.MoveVirtual(arm);

            // try to optimize from that
            adjustWholeArm(arm, trackers);

            // evaluation
            float fn = trackers.CalculateObjFunction();
            if ((fn - f0) < -epsilon)
                f0 = fn;
            else
            {
                arm.arm.transform.position = pastPosition;
                arm.upperarm.eulerAngles = pastArm;
                arm.forearm.eulerAngles = pastForearm;
                trackers.MoveVirtual(arm);
            }
        }
    }

    /// <summary>
    /// Called if position was changed, resets the attribute delta and creates new gradient
    /// </summary>
    /// <param name="arm"> Arm controller </param>
    /// <param name="trackers"> Tracker controller </param>
    internal void NewShift(ArmController arm, TrackerController trackers)
    {
        delta = 5f;
        pastShoulder = arm.arm.transform.position;

        f0 = trackers.CalculateObjFunction();
        f = CalculateGradient(f0, arm, trackers);
    }

    /// <summary>
    /// Set up new arm position as current position plus delta times gradient
    /// </summary>
    /// <param name="arm"> Arm controller </param>
    /// <param name="trackers"> Tracker controller </param>
    /// <returns> Returns true if delta is smaller than epsilon and current position should be deemed optimal, else returns false </returns>
    internal bool TryPosition(ArmController arm, TrackerController trackers)
    {
        if (delta < epsilon)
            return true;

        // shift
        newUpper[0] = MathFunc.Adjust(upperAngles[0] + delta * f[0]);
        newUpper[1] = MathFunc.Adjust(upperAngles[1] + delta * f[1]);
        newUpper[2] = MathFunc.Adjust(upperAngles[2] + delta * f[2]);

        newFor[0] = MathFunc.Adjust(forAngles[0] + delta * f[3]);
        newFor[1] = MathFunc.Adjust(forAngles[1] + delta * f[4]);
        newFor[2] = MathFunc.Adjust(forAngles[2] + delta * f[5]);

        float newXPos = arm.arm.transform.position.x + delta * f[6];
        float newYPos = arm.arm.transform.position.y + delta * f[7];
        float newZPos = arm.arm.transform.position.z + delta * f[8];

        // set new position
        arm.arm.transform.position = new Vector3(newXPos, newYPos, newZPos);
        arm.upperarm.localEulerAngles = new Vector3(newUpper[0], newUpper[1], newUpper[2]);
        arm.forearm.localEulerAngles = new Vector3(newFor[0], newFor[1], newFor[2]);
        trackers.MoveVirtual(arm);

        return false;
    }

    /// <summary>
    /// Evaluates current arm position
    /// </summary>
    /// <param name="arm"> Arm controller </param>
    /// <param name="trackers"> Tracker controller </param>
    /// <returns> Returns false if new position is worse than last accepted, else returns true if current is accepted </returns>
    internal bool EvaluatePosition(ArmController arm, TrackerController trackers)
    {
        float fnew = trackers.CalculateObjFunction();

        if ((fnew - f0) > -epsilon)
        {
            delta /= 2.0f;
            ReturnToLastPosition(arm);
            trackers.MoveVirtual(arm);

            return false;
        }
        else
        {
            upperAngles[0] = newUpper[0];
            upperAngles[1] = newUpper[1];
            upperAngles[2] = newUpper[2];

            forAngles[0] = newFor[0];
            forAngles[1] = newFor[1];
            forAngles[2] = newFor[2];

            return true;
        }
    }

    private void ReturnToLastPosition(ArmController arm)
    {
        arm.arm.transform.position = pastShoulder;
        arm.upperarm.localEulerAngles = new Vector3(upperAngles[0], upperAngles[1], upperAngles[2]);
        arm.forearm.localEulerAngles = new Vector3(forAngles[0], forAngles[1], forAngles[2]); 
    }

    /// <summary>
    /// Creates a new perturbation starting arm position
    /// Slightly changes the angles on the arm to change the position deemed to be optimal
    /// The goal is to get arm from local minimum to a global one
    /// </summary>
    /// <param name="arm"> Arm controller </param>
    /// <param name="trackers"> Tracker controller </param>
    internal void NewPerturbation(ArmController arm, TrackerController trackers)
    {
        f0 = trackers.CalculateObjFunction();
        System.Random r = new System.Random(Config.seed);
        pastShoulder = arm.arm.transform.position;

        arm.forearm.localEulerAngles = new Vector3(arm.forearm.localEulerAngles.x + r.Next(-Config.pertShift, Config.pertShift),
                                                        arm.forearm.localEulerAngles.y + r.Next(-Config.pertShift, Config.pertShift),
                                                        arm.forearm.localEulerAngles.z + r.Next(-Config.pertShift, Config.pertShift));
        arm.upperarm.localEulerAngles = new Vector3(arm.upperarm.localEulerAngles.x + r.Next(-Config.pertShift, Config.pertShift),
                                                         arm.upperarm.localEulerAngles.y + r.Next(-Config.pertShift, Config.pertShift),
                                                         arm.upperarm.localEulerAngles.z + r.Next(-Config.pertShift, Config.pertShift));
        trackers.MoveVirtual(arm);
    }

    /// <summary>
    /// Evaluates the position deemed as optimal, this position originated from a perturbation
    /// </summary>
    /// <param name="arm"> Arm controller </param>
    /// <param name="trackers"> Tracker controller </param>
    /// <returns> True if position is better than the one last accepted, false if not </returns>
    internal bool EvaluatePerturbation(ArmController arm, TrackerController trackers)
    {
        float f = trackers.CalculateObjFunction();
        if ((f - f0) > -epsilon)
        {
            ReturnToLastPosition(arm);
            trackers.MoveVirtual(arm);
            return false;
        }
        else
        {
            f0 = f;
            return true;
        }
    }

    /// <summary>
    /// Executes the whole adjusting process in one frame
    /// Until delta is smaller than epsilon, tries to find a better position than last accepted one
    /// </summary>
    /// <param name="arm"> Arm controller </param>
    /// <param name="trackers"> Tracker controller </param>
    internal void adjustWholeArm(ArmController arm, TrackerController trackers)
    {
        delta = 5f;

        bool newIteration = true;
        while (delta > epsilon)
        {
            if (newIteration)
            {
                NewShift(arm, trackers);
                newIteration = false;
            }

            // shift
            TryPosition(arm, trackers);
            
            // evaluate
            newIteration = EvaluatePosition(arm, trackers);
        }
    }

    /// <summary>
    /// Creates new gradient by evaluating the improvement upon adjusting the position by one step in one degree of freedom at a time
    /// </summary>
    /// <param name="f"> Gradient </param>
    /// <param name="f0"> Value of original position </param>
    /// <param name="arm"> Arm controller </param>
    /// <param name="tracker"> Tracker controller </param>
    /// <returns> Gradient </returns>
    internal float[] CalculateGradient(float f0, ArmController arm, TrackerController tracker)
    {
        float[] f = new float[9];

        // POSITION OF ARM
        Vector3 oldPosArm = arm.arm.transform.position;

        arm.ShiftOnAxis('x');
        tracker.MoveVirtual(arm);
        f[6] = tracker.CalculateObjFunction();

        arm.arm.transform.position = oldPosArm;
        arm.ShiftOnAxis('y');
        tracker.MoveVirtual(arm);
        f[7] = tracker.CalculateObjFunction();

        arm.arm.transform.position = oldPosArm;
        arm.ShiftOnAxis('z');
        tracker.MoveVirtual(arm);
        f[8] = tracker.CalculateObjFunction();

        arm.arm.transform.position = oldPosArm;

        // UPPERARM ANGLES
        Vector3 oldUppper = arm.upperarm.localEulerAngles;

        arm.ShiftOneDegree("upperarm", 'x', upperAngles);
        tracker.MoveVirtual(arm);
        f[0] = tracker.CalculateObjFunction();

        arm.upperarm.localEulerAngles = oldUppper;
        arm.ShiftOneDegree("upperarm", 'y', upperAngles);
        tracker.MoveVirtual(arm);
        f[1] = tracker.CalculateObjFunction();

        arm.upperarm.localEulerAngles = oldUppper;
        arm.ShiftOneDegree("upperarm", 'z', upperAngles);
        tracker.MoveVirtual(arm);
        f[2] = tracker.CalculateObjFunction();

        arm.upperarm.localEulerAngles = oldUppper;

        // FOREARM ANGLES
        Vector3 oldFor = arm.forearm.localEulerAngles;

        arm.ShiftOneDegree("forearm", 'x', forAngles);
        tracker.MoveVirtual(arm);
        f[3] = tracker.CalculateObjFunction();

        arm.forearm.localEulerAngles = oldFor;
        arm.ShiftOneDegree("forearm", 'y', forAngles);
        tracker.MoveVirtual(arm);
        f[4] = tracker.CalculateObjFunction();

        arm.forearm.localEulerAngles = oldFor;
        arm.ShiftOneDegree("forearm", 'z', forAngles);
        tracker.MoveVirtual(arm);
        f[5] = tracker.CalculateObjFunction();

        arm.forearm.localEulerAngles = oldFor;

        f = MathFunc.CreateGradient(f0, f);
        return f;
    }
}