﻿using SimpleFileBrowser;
using System.Collections;
using TMPro;
using UnityEngine;

/// <summary>
/// Class creating and controlling a dialog used for the selection of files and folder on disk
/// </summary>
public class DialogHandler : MonoBehaviour
{
    /// <summary> Input field for hand animation file </summary>
    public TMP_InputField handInput;
    /// <summary> Input field for arm animation file </summary>
    public TMP_InputField armInput;
    /// <summary> Input field for final animation file </summary>
    public TMP_InputField replayInput;
    /// <summary> Input field for data folder </summary>
    public TMP_InputField dataInput;

    /// <summary>
    /// Starts a coroutine opening a file browser window
    /// </summary>
    /// <param name="i">
    ///     0 - replay file
    ///     1 - arm file
    ///     2 - hand file
    ///     3 - data folder
    /// </param>
    internal void OpenDialog(int i)
    {
        StartCoroutine(ShowLoadDialogCoroutine(i));
    }

    /// <summary>
    /// Opens a file browser window
    /// </summary>
    /// <param name="choice">
    ///     Goal of the browser
    ///     0 - replay
    ///     1 - chooose arm animation file
    ///     2 - choose hand animation file
    ///     3 - choose data folder
    /// </param>
    /// <returns> IEnumerator </returns>
    private IEnumerator ShowLoadDialogCoroutine(int choice)
    {
        bool isFolder = false;
        string title = "Select file";

        if (choice == 3)
        {
            isFolder = true;
            title = "Select folder";
        }

        yield return FileBrowser.WaitForLoadDialog(isFolder, Config.InputFolder, title, "Select");

        string path = FileBrowser.Result;
        if (FileBrowser.Success)
        {
            if (choice == 0)
                replayInput.text = path;
            else if (choice == 1)
                armInput.text = path;
            else if (choice == 2)
                handInput.text = path;
            else if (choice == 3)
                dataInput.text = path;
        }
    }
}