﻿using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Class that enables the movement of the camera using keyboard
/// </summary>
public class CameraController : MonoBehaviour
{
    /// <summary> Speed of movement </summary>
    public float movementSpeed;
    /// <summary> Speed of rotation </summary>
    public float rotationSpeed;

    /// <summary> Global vector up </summary>
    private Vector3 constantUp = new Vector3(0, 1, 0);
    /// <summary> Starting position of camera </summary>
    private Vector3 startingPos;
    /// <summary> Starting rotation of camera </summary>
    private Quaternion startingRot;

    /// <summary> Is movement of camera blocked </summary>
    private bool blocked;

    /// <summary>
    /// Executes once at launch
    /// Sets starting position and rotation
    /// </summary>
    private void Start()
    {
        blocked = false;
        startingPos = transform.position;
        startingRot = transform.rotation;
    }

    /// <summary>
    /// Executes every update
    /// Calls methods which provide reactions on pressed keys
    /// </summary>
    private void Update()
    {
        // if other object is active or if camera movement is blocked
        if ( (EventSystem.current.currentSelectedGameObject != null) || (blocked) )
            return;

        // actions
        ResetListener();
        MovementListener();
        RotationListener();
    }

    /// <summary>
    /// Controls the rotation of the camera
    /// Reacts on:
    ///     - left shift - speeds up rotation
    ///     - up arrow - rotates upwards
    ///     - down arrow - rotates downwards
    ///     - left arrow - rotates left
    ///     - right arrow - rotates right
    /// </summary>
    private void RotationListener()
    {
        float rotationX = transform.localEulerAngles.x;
        float rotationY = transform.localEulerAngles.y;

        float rotation = (Input.GetKey(KeyCode.LeftShift)) ? rotationSpeed * 2 : rotationSpeed;

        if (Input.GetKey(KeyCode.UpArrow))
            rotationX = (rotationX - (rotation * Time.deltaTime));
        if (Input.GetKey(KeyCode.DownArrow))
            rotationX = (rotationX + (rotation * Time.deltaTime));
        if (Input.GetKey(KeyCode.LeftArrow))
            rotationY = (rotationY - (rotation * Time.deltaTime)) % 360;
        if (Input.GetKey(KeyCode.RightArrow))
            rotationY = (rotationY + (rotation * Time.deltaTime)) % 360;

        if (rotationX > 180)
            rotationX = Mathf.Max(rotationX, 271);
        else
            rotationX = Mathf.Min(rotationX, 89);

        transform.localEulerAngles = new Vector3(rotationX, rotationY, 0f);
    }

    /// <summary>
    /// Controls the movement of the camera
    /// Reacts on:
    ///     - left shift
    ///     - w - moves forward
    ///     - a - moves left
    ///     - s - moves down
    ///     - d - moves right
    ///     - q - moves up
    ///     - e - moves down
    /// </summary>
    private void MovementListener()
    {
        Vector3 newPosition = transform.position;

        float movement = (Input.GetKey(KeyCode.LeftShift)) ? movementSpeed * 2 : movementSpeed;

        if (Input.GetKey(KeyCode.W))
            newPosition += transform.forward * movement * Time.deltaTime;
        if (Input.GetKey(KeyCode.A))
            newPosition -= transform.right * movement * Time.deltaTime;
        if (Input.GetKey(KeyCode.S))
            newPosition -= transform.forward * movement * Time.deltaTime;
        if (Input.GetKey(KeyCode.D))
            newPosition += transform.right * movement * Time.deltaTime;
        if (Input.GetKey(KeyCode.Q) && !Input.GetKey(KeyCode.LeftControl))
            newPosition += constantUp * movement * Time.deltaTime;
        if (Input.GetKey(KeyCode.E))
            newPosition -= constantUp * movement * Time.deltaTime;

        gameObject.transform.position = newPosition;
    }

    /// <summary>
    /// Blocks/unblocks camera movement
    /// </summary>
    internal void ToggleBlock(bool val)
    {
        blocked = val;
    }

    /// <summary>
    /// Resets the rotation and position of the camera
    /// Reacts on:
    ///     - r - resets camera rotation and position
    /// </summary>
    private void ResetListener()
    {
        if (Input.GetKey(KeyCode.R))
        {
            transform.position = startingPos;
            transform.rotation = startingRot;
            return;
        }
    }
}