﻿using System;
using System.Collections.Generic;
using System.Globalization;

/// <summary>
/// Controlls the generation of animation frames
/// The generation of one frame is divide between more Unity FixedUpdates, as to avoid the applicaiont not responding
/// </summary>
public class AnimationController
{
    /// <summary> Tracker controller </summary>
    private TrackerController trackers;
    /// <summary> Arm controller </summary>
    private ArmController arm;
    /// <summary> Adjust controller </summary>
    private AdjustController adjustCont;

    /// <summary> Time to load new frame from data </summary>
    private bool newDataFrame;
    /// <summary> Create new perturbation from accepted arm position </summary>
    private bool newPertFrame;
    /// <summary> Evaluate the results of a perturbation </summary>
    private bool evalPert;
    /// <summary> Adjusting position </summary>
    private bool adjusting;
    /// <summary> Adjusting position created in a perturbation </summary>
    private bool adjustingPert;
    /// <summary> Was a new arm position accepted </summary>
    private bool moved;

    /// <summary> How many perturbations were already done </summary
    private int pertTimes;

    /// <summary> Output to be written into file, stores animation frames </summary>
    internal string[] result;
    /// <summary> Index of first free space in result </summary>
    private int index;

    /// <summary> Input data from chest tracker </summary>
    internal Queue<Position> chestData;

    /// <summary> Input data from forearm tracker </summary>
    private Queue<Position> forData;
    /// <summary> Input data from upperarm tracker </summary>
    private Queue<Position> upperData;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="trackers"> Tracker controller </param>
    /// <param name="arm"> Arm controller </param>
    /// <param name="adjustCont"> Adjust controller </param>
    internal AnimationController(TrackerController trackers, ArmController arm, AdjustController adjustCont)
    {
        this.trackers = trackers;
        this.arm = arm;
        this.adjustCont = adjustCont;

        arm.InstantiateArm(Config.right);
        trackers.InstantiateTrackers();
        adjustCont.StartAngles(arm);
    }

    /// <summary>
    /// Stop displaying trackers and arm
    /// </summary>
    internal void Stop()
    {
        trackers.Stop();
        arm.Stop();
    }

    /// <summary>
    /// Continue calculating next frame of animation from where it was left off last update
    /// </summary>
    /// <returns> True if finished animating, false if not </returns>
    internal bool NextFrame()
    {
        // read new data frame
        if (newDataFrame)
        {
            trackers.PositionChest(chestData);
            arm.PositionArm(chestData, trackers, true);
            trackers.MoveLeading(forData, upperData, true);
            trackers.MoveVirtual(arm);

            newDataFrame = false;
            adjusting = true;
            moved = true;
        }

        // trying to find best suited position
        if (adjusting || adjustingPert)
        {
            bool end = false;
            while (!end)
            {

                // make new gradient
                if (moved)
                {
                    moved = false;
                    adjustCont.NewShift(arm, trackers);
                }

                // trying to shift
                end = adjustCont.TryPosition(arm, trackers);
                if (end)
                {
                    if (adjusting)
                    {
                        newPertFrame = true;
                        pertTimes = 0;
                    }
                    else if (adjustingPert)
                        evalPert = true;

                    adjusting = false;
                    adjustingPert = false;
                }

                // evaluate new position
                moved = adjustCont.EvaluatePosition(arm, trackers);
            }
        }

        // done with previous perturbation
        if (newPertFrame)
        {
            // finished
            if (pertTimes == Config.pertTimes)
            {
                Write();
                newDataFrame = true;

                if (chestData.Count == 0)
                {
                    Stop();
                    return true;
                }
            }
            // new perturbation
            else
            {
                adjustCont.NewPerturbation(arm, trackers);
                pertTimes++;
                adjustingPert = true;
            }

            newPertFrame = false;
        }

        // evaluate perturbation
        if (evalPert)
        {
            bool res = adjustCont.EvaluatePerturbation(arm, trackers);
            newPertFrame = true;
            evalPert = false;
        }

        return false;
    }

    /// <summary>
    /// Write animation frame to results
    /// </summary>
    private void Write()
    {
        System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
        result[index] = arm.upperarm.eulerAngles.x + "; " + arm.upperarm.eulerAngles.y + "; " + arm.upperarm.eulerAngles.z + "; "
                      + arm.forearm.eulerAngles.x + "; " + arm.forearm.eulerAngles.y + "; " + arm.forearm.eulerAngles.z;
        index++;
    }

    /// <summary>
    /// Do all actions needed before the first frame of animation
    /// </summary>
    /// <returns> True if executed sucessfully, false if not </returns>
    internal bool SetUp()
    {
        if (!FileManip.Exists(Config.DataFolderPath + "/" + Config.chestData) ||
            !FileManip.Exists(Config.DataFolderPath + "/" + Config.forearmData) ||
            !FileManip.Exists(Config.DataFolderPath + "/" + Config.armData))
            return false;

        ReadData();
        SetUpArmPosition();

        index = 0;
        result = new string[chestData.Count];
        newDataFrame = true;

        return true;
    }
    
    /// <summary>
    /// Set up arm position in space before animating
    /// </summary>
    private void SetUpArmPosition()
    {
        trackers.PositionChest(chestData);
        arm.PositionArm(chestData, trackers, false);
        trackers.MoveVirtual(arm);
        trackers.MoveLeading(forData, upperData, false);
        adjustCont.RandFirstPos(arm, trackers);
    }

    /// <summary>
    /// Read input data 
    /// </summary>
    internal void ReadData()
    {
        chestData = DataReader.ReadDataFile(Config.DataFolderPath + "/" + Config.chestData);
        forData = DataReader.ReadDataFile(Config.DataFolderPath + "/" + Config.forearmData);
        upperData = DataReader.ReadDataFile(Config.DataFolderPath + "/" + Config.armData);
    }
}