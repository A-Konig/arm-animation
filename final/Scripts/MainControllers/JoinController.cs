﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Joins two animation files
/// </summary>
public class JoinController : MonoBehaviour
{
    /// <summary> Arm controller </summary>
    public ArmController arm;

    /// <summary> Data read from hand animation file </summary>
    internal Queue<Position> tails;
    /// <summary> Lines from arm animation file </summary>
    private string[] armLines;
    /// <summary> Lines from hand animation file </summary>
    private string[] handLines;
    /// <summary> Lines to be written into output animation file </summary>
    private string[] result;

    /// <summary> Index of last free position in result </summary>
    int index;
    /// <summary> Path to output file </summary>
    private string outputFile;

    /// <summary> Is currently joining animation </summary>
    internal bool joining = false;

    /// <summary>
    /// Executes every frame
    /// Calls method processing new dataframe if not done processing input frames,
    /// or calls method joining the two animations together and writing result into outputfile
    /// </summary>
    private void Update()
    {
        // currently joining
        if (joining)
        {
            // all lines considered as processed
            if (index >= handLines.Length)
            {
                joining = false;

                JoinFiles();
                DataWriter.WriteAllToFile(result, outputFile);

                return;
            }

            // process next line from input
            arm.MoveHand(tails);
            WriteToHandLines();
        }
    }

    /// <summary>
    /// Writes animation frame to handLines
    /// </summary>
    private void WriteToHandLines()
    {
        Transform[] allChildren = arm.hand.GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            handLines[index] = $"{child.localEulerAngles.x};{child.localEulerAngles.y};{child.localEulerAngles.z}";
            index++;
        }
    }

    /// <summary>
    /// Joins armFile and handFile lines together into result
    /// </summary>
    private void JoinFiles()
    {
        FileManip.RemoveFile(outputFile);

        int armLinesCount = armLines.GetLength(0);
        int handLinesCount = handLines.GetLength(0);
        handLinesCount = handLinesCount / Config.bonescount;

        result = new string[armLinesCount + armLinesCount * Config.bonescount];

        // for each frame in armFile find corresponding frame in handFile
        int outIndex = 0;
        for (int i = 0; i < armLinesCount; i++)
        {
            result[outIndex] = armLines[i];
            outIndex++;

            int index = (int)Mathf.Round(i * (float)(handLinesCount - 1) / (armLinesCount - 1));
            for (int j = 0; j < Config.bonescount; j++)
            {
                result[outIndex] = handLines[(index * Config.bonescount) + j];
                outIndex++;
            }
        }
    }

    /// <summary>
    /// Reads animation files
    /// </summary>
    /// <param name="armFile"> Arm animation file </param>
    /// <param name="handFile"> Hand animation file </param>
    /// <returns> True if loaded correctly, false if not </returns>
    private bool ReadData(string armFile, string handFile)
    {
        armLines = DataReader.ReadAllLines(armFile);

        // is handFile correct input
        string[] handLines = DataReader.ReadAllLines(handFile);
        if (!FileManip.IsArmFile(armLines) || !FileManip.IsHandFile(handLines))
            return false;

        tails = DataReader.ReadBlenderAnimation(handFile);
        return true;
    }

    /// <summary>
    /// Starts the joining of animation files into one
    /// </summary>
    /// <param name="armFile"> Path to arm animation file </param>
    /// <param name="handFile"> Path to hand animation file </param>
    /// <param name="outputFile"> Path to output file </param>
    /// <returns> True if executed successfully, false if not </returns>
    internal bool StartJoin(string armFile, string handFile, string outputFile)
    {
        // correct input
        if (!FileManip.Exists(armFile) || !FileManip.Exists(handFile))
            return false;

        index = 0;
        this.outputFile = outputFile;

        // read data
        bool res = ReadData(armFile, handFile);
        if (!res)
            return false;
        handLines = new string[tails.Count];

        joining = true;
        arm.InstantiateArm(Config.right);
        return true;
    }

    /// <summary>
    /// Stops joining animations
    /// </summary>
    internal void Stop()
    {
        joining = false;
        arm.Stop();
    }
}