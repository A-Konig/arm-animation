﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controlls the replaying of an animation
/// </summary>
public class ReplayController : MonoBehaviour
{
    /// <summary> Arm controller </summary>
    public ArmController arm;

    /// <summary> Is currently playing animation </summary>
    internal bool replay;
    /// <summary> Queue with animation data, contents depends on type of animation </summary>
    private Queue<Position> animationData;
    /// <summary> The type of animation being played
    ///  1 - arm animation
    ///  2 - hand animation
    ///  3 - final animation
    /// <summary>
    private int type; 
    
    /// Called before the first frame update, sets up attributes
    /// </summary>
    void Start()
    {
        replay = false;
        animationData = null;
    }

    /// <summary>
    /// Called every frame
    /// If replaying calls method handling the visualisation of the frame of the animation
    /// </summary>
    void Update()
    {
        if (replay)
            replay = ReplayHandling();
    }

    /// <summary>
    /// Displays next frame of animation
    /// </summary>
    /// <returns> True if some remain to be displayed, false if all frames has been displayed </returns>
    internal bool ReplayHandling()
    {
        if (animationData != null)
        {
            if (animationData.Count == 0)
                return false;

            Position pos;
            // if hand animation
            if (type == 2)
            {
                arm.MoveHand(animationData);
            }
            // if arm or final animation
            else
            {
                pos = animationData.Peek();
                arm.upperarm.eulerAngles = new Vector3(pos.rotation.x, pos.rotation.y, pos.rotation.z);
                animationData.Dequeue();

                if (animationData.Count == 0)
                    return false;

                pos = animationData.Peek();
                arm.forearm.eulerAngles = new Vector3(pos.rotation.x, pos.rotation.y, pos.rotation.z);
                animationData.Dequeue();

                if (animationData.Count == 0)
                    return false;

                // not done if arm animation
                if (type != 1)
                {
                    Transform[] allChildren = arm.hand.GetComponentsInChildren<Transform>();
                    foreach (Transform child in allChildren)
                    {
                        pos = animationData.Peek();


                        child.localEulerAngles = new Vector3(pos.rotation.x, pos.rotation.y, pos.rotation.z);
                        animationData.Dequeue();

                        if (animationData.Count == 0)
                            return false;
                    }
                }
            }

            // no remaining frames
            if (animationData.Count == 0)
                return false;
        }

        return true;
    }

    /// <summary>
    /// Starts the replaying of an animation 
    /// </summary>
    /// <param name="path"> File with animation </param>
    /// <returns> True if executed successfully, false if not </returns>
    internal bool StartReplay(string path)
    {
        replay = true;

        // get type of animation file and read data
        type = FileManip.GetAnimationType(path);
        switch (type)
        {
            case -1:
                replay = false;
                return false;
            case 1:
                animationData = DataReader.ReadArmAnimation(path);
                break;
            case 2:
                animationData = DataReader.ReadBlenderAnimation(path);
                break;
            case 3:
                animationData = DataReader.ReadFinalAnimation(path);
                break;
        }

        if (animationData == null)
        {
            replay = false;
            return false;
        }

        arm.InstantiateArm(Config.right);
        return true;
    }

    /// <summary>
    /// Stops replaying animation
    /// </summary>
    internal void Stop()
    {
        arm.Stop();
        replay = false;
    }
}
