﻿using UnityEngine;

/// <summary>
/// Controlls the flow of generating new arm animation
/// </summary>
public class GenerateController : MonoBehaviour
{
    /// <summary> Tracker controller </summary>
    public TrackerController trackers;
    /// <summary> Arm controller </summary>
    public ArmController arm;
    /// <summary> Adjust controller </summary>
    public AdjustController adjustCont;

    /// <summary> Animation controller </summary>
    internal AnimationController animCont;

    /// <summary> Name of the output file </summary>
    private string outputFile;

    /// <summary> Is currently generating </summary>
    internal bool generate = false;
    /// <summary> Is done animating and is the animation waiting to be replayed </summary>
    internal bool toReplay = false;
    /// <summary> Is finished with generating </summary>
    private bool finished = false;

    /// <summary>
    /// Executed every frame
    /// Calls method generating another animation frame if not done with generating, or method writeing the result into outputfile
    /// </summary>
    private void Update()
    {
        // is currently generating
        if (generate)
        {
            // done generating, process output
            if (finished)
            {
                finished = false;
                generate = false;

                string[] result = animCont.result;
                DataWriter.WriteAllToFile(result, outputFile);
                toReplay = true;

            // generate next frame
            } else
                finished = animCont.NextFrame();
        }
    }

    /// <summary>
    /// Stops generating
    /// </summary>
    internal void Stop()
    {
        animCont.Stop();
        generate = false;
    }

    /// <summary>
    /// Start generating new arm animation
    /// </summary>
    /// <returns> True if executed successfully, false if not </returns>
    internal bool StartGeneration()
    {
        generate = true;

        animCont = new AnimationController(trackers, arm, adjustCont);
        if (!animCont.SetUp())
        {
            generate = false;
            animCont.Stop();
            return false;
        }

        outputFile = Config.OutputFile;
        FileManip.RemoveFile(outputFile);

        return true;
    }
}