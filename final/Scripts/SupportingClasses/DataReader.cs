﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;

/// <summary>
/// Reads input data
/// </summary>
public static class DataReader
{
    /// <summary>
    /// Returns data read from arm data file.
    /// </summary>
    /// <param path="path"> path to file </param>
    /// <returns> queue with data, each two instances of Position represent rotation of bones in arm in one frame </returns>
    public static Queue<Position> ReadDataFile(string path)
    {
        // is it valid file
        if (!FileManip.Exists(path))
            return null;

        Queue<Position> res = new Queue<Position>();
        string[] lines = File.ReadAllLines(@path);

        // process all lines
        for (int i = 0; i < lines.Length; i++)
        {
            string[] words = lines[i].Split(';');

            if (words.Length == Config.columnNumber)
            {
                try
                {
                    res.Enqueue(new Position(float.Parse(words[1].Trim(), CultureInfo.InvariantCulture),
                                             float.Parse(words[2].Trim(), CultureInfo.InvariantCulture),
                                             float.Parse(words[3].Trim(), CultureInfo.InvariantCulture),
                                             float.Parse(words[4].Trim(), CultureInfo.InvariantCulture),
                                             float.Parse(words[5].Trim(), CultureInfo.InvariantCulture),
                                             float.Parse(words[6].Trim(), CultureInfo.InvariantCulture)));
                } catch
                {
                    res.Enqueue(new Position(0, 0, 0, 0, 0, 0));
                }
            }
        }

        return res;
    }

    /// <summary>
    /// Returns animation data read from final animation file
    /// </summary>
    /// <param name="path"> path to file </param>
    /// <returns></returns>
    public static Queue<Position> ReadFinalAnimation(string path)
    {
        Queue<Position> res = new Queue<Position>();

        // read all lines
        string[] lines = File.ReadAllLines(path);

        // number of frames in file
        int sets = lines.Length / (Config.bonescount + 1);

        // process all lines
        for (int i = 0; i < sets; i++)
        {
            int index = i * (Config.bonescount + 1);
            string[] words = lines[index].Split(';');

            // error in file format
            if (words.Length != Config.columnNumberArm)
                return null;

            // arm animation
            try {
                Position p1 = new Position(0, 0, 0,
                                 float.Parse(words[0].Trim(), CultureInfo.InvariantCulture),
                                 float.Parse(words[1].Trim(), CultureInfo.InvariantCulture),
                                 float.Parse(words[2].Trim(), CultureInfo.InvariantCulture)
                                );
                Position p2 = new Position(0, 0, 0,
                                 float.Parse(words[3].Trim(), CultureInfo.InvariantCulture),
                                 float.Parse(words[4].Trim(), CultureInfo.InvariantCulture),
                                 float.Parse(words[5].Trim(), CultureInfo.InvariantCulture)
                                );

                res.Enqueue(p1);
                res.Enqueue(p2);
            }
            catch
            {
                res.Enqueue(new Position(0, 0, 0, 0, 0, 0));
                res.Enqueue(new Position(0, 0, 0, 0, 0, 0));
            }

            // hand animation
            for (int j = 1; j <= Config.bonescount; j++)
            {
                words = lines[index + j].Split(';');

                // error in file format
                if (words.Length != Config.columnUnityHand)
                    return null;

                try { 
                    res.Enqueue(
                        new Position(0, 0, 0,
                                     float.Parse(words[0].Trim(), CultureInfo.InvariantCulture),
                                     float.Parse(words[1].Trim(), CultureInfo.InvariantCulture),
                                     float.Parse(words[2].Trim(), CultureInfo.InvariantCulture)
                                    ));
                }
                catch
                {
                    res.Enqueue(new Position(0, 0, 0, 0, 0, 0));
                }
            }
        }

        return res;
    }

    /// <summary>
    /// Returns data read from hand animation file exported from Blender
    /// </summary>
    /// <param name="path"> Path to file </param>
    /// <returns> Animation data </returns>
    public static Queue<Position> ReadBlenderAnimation(string path)
    {
        Queue<Position> res = new Queue<Position>();

        // read all lines
        string[] lines = File.ReadAllLines(path);

        // is it valid file format
        if ((lines.Length-1) % Config.bonescount != 0)
            return null;

        for (int i = 1; i < lines.Length; i++)
        {
            string[] words = lines[i].Split(';');

            if (words.Length != Config.columnBlenderHand)
                return null;

            try
            {
                Vector3 pos = new Vector3(float.Parse(words[0], CultureInfo.InvariantCulture),
                                          float.Parse(words[1], CultureInfo.InvariantCulture),
                                          float.Parse(words[2], CultureInfo.InvariantCulture));

                Vector3 rot = new Vector3(float.Parse(words[3], CultureInfo.InvariantCulture),
                                          float.Parse(words[4], CultureInfo.InvariantCulture),
                                          float.Parse(words[5], CultureInfo.InvariantCulture));

                res.Enqueue(new Position(pos, rot));
            } catch
            {
                res.Enqueue(new Position(0, 0, 0, 0, 0, 0));
            }
        }
        
        return res;
    }

    /// <summary>
    /// Reads animation data from arm animation file
    /// </summary>
    /// <param name="path"> Path to file </param>
    /// <returns></returns>
    public static Queue<Position> ReadArmAnimation(string path)
    {
        Queue<Position> res = new Queue<Position>();

        // read all lines
        string[] lines = File.ReadAllLines(path);

        // process all lines
        for (int i = 0; i < lines.Length; i++)
        {
            string[] words = lines[i].Split(';');

            // error in file format
            if (words.Length != Config.columnNumberArm)
                return null;

            // arm animation
            try
            {
                Position p1 = new Position(0, 0, 0,
                                     float.Parse(words[0].Trim(), CultureInfo.InvariantCulture),
                                     float.Parse(words[1].Trim(), CultureInfo.InvariantCulture),
                                     float.Parse(words[2].Trim(), CultureInfo.InvariantCulture)
                                    );
                Position p2 = new Position(0, 0, 0,
                                     float.Parse(words[3].Trim(), CultureInfo.InvariantCulture),
                                     float.Parse(words[4].Trim(), CultureInfo.InvariantCulture),
                                     float.Parse(words[5].Trim(), CultureInfo.InvariantCulture)
                                    );

                res.Enqueue(p1);
                res.Enqueue(p2);
            } catch
            {
                res.Enqueue(new Position(0, 0, 0, 0, 0, 0));
                res.Enqueue(new Position(0, 0, 0, 0, 0, 0));
            }
        }

        return res;
    }

    /// <summary>
    /// Reads all lines from file
    /// </summary>
    /// <param name="path"> path to file </param>
    /// <returns> array with lines </returns>
    public static string[] ReadAllLines(string path)
    {
        return File.ReadAllLines(@path);
    }
}