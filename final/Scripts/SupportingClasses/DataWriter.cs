﻿using System.IO;

/// <summary>
/// Class providing methods to write into a file and to create new filenames
/// </summary>
public static class DataWriter
{
    /// <summary>
    /// Writes all lines to file at path
    /// </summary>
    /// <param name="lines"> Lines </param>
    /// <param name="path"> Path to file </param>
    public static void WriteAllToFile(string[] lines, string path)
    {
        if (!Directory.Exists(Config.OutputFolder))
            Directory.CreateDirectory(Config.OutputFolder);

        StreamWriter file = new StreamWriter(path, true);
        for (int i = 0; i < lines.Length; i++)
            file.WriteLine(lines[i]);

        file.Flush();
        file.Close();
    }

    /// <summary>
    /// Creates the name of output arm animation from data file
    /// </summary>
    /// <param name="path"> Path to folder with input </param>
    /// <returns> Name of file </returns>
    public static string CreateAnimationFile(string path)
    {
        return $"{Path.GetFileName(path)}.csv";
    }

    /// <summary>
    /// Creates the name of output final animation file
    /// </summary>
    /// <param name="pathArm"> Path to file with arm animation </param>
    /// <param name="pathHand"> Path to file with hand animation </param>
    /// <returns> Name of file </returns>
    public static string CreateFinalFile(string pathArm, string pathHand)
    {
        string f1 = FileManip.GetName(pathArm);
        string f2 = FileManip.GetName(pathHand);
        return $"{f1}_{f2}.csv";
    }
}
