﻿using UnityEngine;

/// <summary>
/// Position and rotation in world space
/// </summary>
public class Position
{
    /// <summary> Position </summary>
    internal Vector3 position;
    /// <summary> Rotation </summary>
    internal Vector3 rotation;

    /// <summary>
    /// Constructior
    /// </summary>
    /// <param name="x"> Position x coordinate </param>
    /// <param name="y"> Position y coordinate </param>
    /// <param name="z"> Position z coordinate </param>
    /// <param name="rotX"> Rotation x coordinate </param>
    /// <param name="rotY"> Rotation y coordinate </param>
    /// <param name="rotZ"> Rotation z coordinate </param>
    internal Position(float x, float y, float z, float rotX, float rotY, float rotZ)
    {
        position = new Vector3(x, y, z);
        rotation = new Vector3(rotX, rotY, rotZ);
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="position"> Position in space </param>
    /// <param name="rotation"> Rotation in space </param>
    internal Position(Vector3 position, Vector3 rotation)
    {
        this.position = position;
        this.rotation = rotation;
    }

}