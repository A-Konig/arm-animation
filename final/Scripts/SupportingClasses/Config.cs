﻿using UnityEngine;

/// <summary>
/// Configuration file
/// </summary>
public static class Config
{
    /// <summary> Number of columns in data file </summary>
    public readonly static int columnNumber = 7;
    /// <summary> Number of columns in arm animation file </summary>
    public readonly static int columnNumberArm = 6;
    /// <summary> Number of columns in a hand bone data in final animation file </summary>
    public readonly static int columnUnityHand = 3;
    /// <summary> Number of columns in hand animation file </summary>
    public readonly static int columnBlenderHand = 6;

    /// <summary> Number of bones in hand </summary>
    public readonly static int bonescount = 21;

    /// <summary> Number of perturbations </summary>
    public readonly static int pertTimes = 100;
    /// <summary> Max value of shift in a perturbation </summary>
    public readonly static int pertShift = 15;

    /// <summary> Seed for random </summary>
    public readonly static int seed = 42;
    /// <summary> Number of frames in a hand animation </summary>
    internal readonly static int frameNum = 200;

    /// <summary> Is right arm selected </summary>
    public static bool right;

    /// <summary> Path to armature in tree </summary>
    public readonly static string armature = "Armature";
    /// <summary> Path to first hand bone in tree </summary>
    public readonly static string handBone = "Armature/Bone/Bone_001/Bone_002";
    /// <summary> Path to upperarm bone in tree </summary>
    public readonly static string upperarmBone = "Armature/Bone";
    /// <summary> Path to forearm bone in tree </summary>
    public readonly static string forearmBone = "Armature/Bone/Bone_001";

    /// <summary> Name of file with forearm tracker data </summary>
    public readonly static string forearmData = "Forearm.csv";
    /// <summary> Name of file with arm tracker data </summary>
    public readonly static string armData = "Arm.csv";
    /// <summary> Name of file with chest tracker data </summary>
    public readonly static string chestData = "Chest.csv";
    /// <summary> Name of file with headset tracker data </summary>
    public readonly static string headData = "Head.csv";

    /// <summary>
    /// Path to data folder
    /// </summary>
    private static string dataFolderPath;
    public static string DataFolderPath { get => dataFolderPath; set => dataFolderPath = value; }

    /// <summary>
    /// Path to output file
    /// </summary>
    private static string outputFile;
    public static string OutputFile { get => outputFile; set => outputFile = outputFolder + value; }

    /// <summary>
    /// Path to replay file
    /// </summary>
    private static string replayPath;
    public static string ReplayPath { get => replayPath; set => replayPath = value; }

    /// <summary>
    /// Path to arm animation file
    /// </summary>
    private static string armAnimationFile;
    public static string ArmAnimationFile { get => armAnimationFile; set => armAnimationFile = value; }

    /// <summary>
    /// Path to hand animation file
    /// </summary>
    private static string handAnimationFile;
    public static string HandAnimationFile { get => handAnimationFile; set => handAnimationFile = value; }

    /// <summary>
    /// Path to output folder
    /// </summary>
    private static string outputFolder;
    public static string OutputFolder
    {
        get
        {
            return outputFolder;
        }
        set
        {
            string val = Application.dataPath;

            if (val[val.Length - 1] == '/' || val[val.Length - 1] == '\\')
                val = val.Substring(0, val.Length - 1);

            string path = "";
            for (int i = val.Length - 1; i > -1; i--)
                if (val[i] == '/' || val[i] == '\\')
                {
                    path = val.Substring(0, i);
                    break;
                }

            outputFolder = path + "/output/";
        }
    }

    /// <summary>
    /// Path to default input folder
    /// </summary>
    private static string inputFolder;
    public static string InputFolder
    {
        get
        {
            return inputFolder;
        }

        set
        {
            string val = Application.dataPath;

            if (val[val.Length - 1] == '/' || val[val.Length - 1] == '\\')
                val = val.Substring(0, val.Length - 1);

            string path = "";
            for (int i = val.Length - 1; i > -1; i--)
                if (val[i] == '/' || val[i] == '\\')
                {
                    path = val.Substring(0, i);
                    break;
                }

            inputFolder = path;
        }
    }
}