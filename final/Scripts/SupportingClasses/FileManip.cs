﻿using System.IO;

/// <summary>
/// Class for the manipulation with files
/// Provides methods for loading data and for the manipulation with filenames and files
/// </summary>
public static class FileManip
{
    /// <summary>
    /// Is animation data in lines accepted arm animation file
    /// </summary>
    /// <param name="lines"> Animation data </param>
    /// <returns> True if it is arm animation file, false if not </returns>
    public static bool IsArmFile(string[] lines)
    {
        for (int i = 0; i < lines.Length; i++)
        {
            string[] words = lines[i].Split(';');
            if (words.Length != Config.columnNumberArm)
                return false;
        }

        return true;
    }

    /// <summary>
    /// Is animation data in lines accepted hand animation file
    /// </summary>
    /// <param name="lines"> Animation data </param>
    /// <returns> True if it is hand animation file, false if not </returns>
    public static bool IsHandFile(string[] lines)
    {
        if (!lines[0].Equals("//HandAnim"))
            return false;

        for (int i = 1; i < lines.Length; i++)
        {
            string[] words = lines[i].Split(';');
            if (words.Length != Config.columnBlenderHand)
                return false;
        }

        if ((lines.Length-1) % Config.bonescount != 0)
            return false;
        else
            return true;
    }

    /// <summary>
    /// Is animation data in lines accepted final animation file
    /// </summary>
    /// <param name="lines"> Animation data </param>
    /// <returns> True if it is final animation file, false if not </returns>
    public static bool IsFinalFile(string[] lines)
    {
        if (lines.Length % (Config.bonescount + 1) != 0)
            return false;
        return true;
    }

    /// <summary>
    /// Determine the type of animation file
    /// </summary>
    /// <param name="path"> Paht to file </param>
    /// <returns></returns>
    public static int GetAnimationType(string path)
    {
        if (!Exists(path))
            return -1;

        string[] lines = File.ReadAllLines(path);

        if (IsArmFile(lines))
            return 1;
        if (IsHandFile(lines))
            return 2;
        if (IsFinalFile(lines))
            return 3;
        return -1;
    }

    /// <summary>
    /// Checks if file exists and its extension is .csv
    /// </summary>
    /// <param name="path"> Path to file </param>
    /// <returns> True if file is in correct format or false if not </returns>
    public static bool Exists(string path)
    {
        if (!File.Exists(path) || !Path.GetExtension(path).Equals(".csv"))
            return false;
        return true;
    }

    /// <summary>
    /// Gets the name of a file at path, with or without extension
    /// </summary>
    /// <param name="path"> Path to file </param>
    /// <param name="ext"> True if with extension, false if not, set to false on default </param>
    /// <returns> Name of file </returns>
    public static string GetName(string path, bool ext = false)
    {
        if (ext)
            return Path.GetFileName(path);
        return Path.GetFileNameWithoutExtension(path);
    }

    /// <summary>
    /// Removes file from directory
    /// </summary>
    /// <param name="path"> Path to file </param>
    public static void RemoveFile(string path)
    {
        if (File.Exists(path))
            File.Delete(path);
    }
}