﻿using System;
using UnityEngine;

/// <summary>
/// Contains mathematical functions used in the application
/// </summary>
public class MathFunc
{
    /// <summary>
    /// Transforms angle val to <-180,180>
    /// </summary>
    /// <param name="val"> Value of angle in degrees </param>
    /// <returns></returns>
    public static float Adjust(float val)
    {
        float res = val;

        if (val > 180)
        {
            val = val % 360;
            res = -180 + (val - 180);
        }
        else if (val < -180)
        {
            val = val % 360;
            res = 180 + (val + 180);
        }

        return res;
    }

    /// <summary>
    /// Edits Blender coordinates to Unity
    /// </summary>
    /// <param name="pos"> Vector to edit </param>
    /// <returns> Vector in Unity coordinates </returns>
    internal static Vector3 BlendToUnity(Vector3 vect)
    {
        vect.x = -vect.x;
        return vect;
    }

    /// <summary>
    /// Calculates the distance squared between two given transforms
    /// </summary>
    /// <param name="t1"> Transform </param>
    /// <param name="t2"> Transform </param>
    /// <returns> Distance squared </returns>
    public static float DistanceSqr(Transform t1, Transform t2)
    {
        float dist = Vector3.SqrMagnitude(t2.localPosition - t1.localPosition); //sqrmagnitude
        return dist;
    }

    /// <summary>
    /// Calculates the difference of the direction of the axis axis between two transforms
    /// </summary>
    /// <param name="t1"> Transform </param>
    /// <param name="t2"> Transform </param>
    /// <param name="axis"> Axis to compute the difference of, accepted values:7
    ///     'x' - x axis
    ///     'y' - y axis
    ///     'z' - z axis
    /// </param>
    /// <returns> Difference between the axis in degrees </returns>
    internal static double AngleDiff(Transform t1, Transform t2, char axis)
    {
        // axis in world coordinates
        Vector3 v = new Vector3(0, 0, 0);
        Vector3 u = new Vector3(0, 0, 0);
        double angle = 0;

        if (axis == 'x')
        {
            u = t1.transform.right;
            v = t2.transform.right;
        }
        else if (axis == 'y')
        {
            u = t1.transform.up;
            v = t2.transform.up;
        }
        else if (axis == 'z')
        {
            u = t1.transform.forward;
            v = t2.transform.forward;
        }

        // get angle difference
        double d = (u.magnitude * v.magnitude);
        if (d != 0)
        {
            // dot product divided by magnitudes
            var c = (u.x * v.x + u.y * v.y + u.z * v.z) / d;
            if (c < -1) c = -1;
            if (c > 1) c = 1;
            angle = Math.Acos(c) * Mathf.Rad2Deg;
        }

        return angle;
    }

    /// <summary>
    /// Calculates the value of the optimalisation function on current configuration of trackers
    /// </summary>
    /// <param name="virtualUpper"> Virtual upperarm tracker </param>
    /// <param name="leadingUpper"> Leading object for virtual upperarm tracker </param>
    /// <param name="virtualFor"> Virtual forearm tracker </param>
    /// <param name="leadingFor"> Leading object for virtual forearm tracker </param>
    /// <returns> Value </returns>
    public static float CalculateObjFunc(Transform virtualUpper, Transform leadingUpper, Transform virtualFor, Transform leadingFor)
    {
        float a = 0.1f;
        float b = 0.1f;
        float c = 0.1f;
        float f = 0.1f;

        // f * distanceSqr + a * angleDiff(x) + b * angleDiff(y) + c * angleDiff(z)
        float weightShoulder = (float)(f * MathFunc.DistanceSqr(virtualUpper.transform, leadingUpper)
                             + a * MathFunc.AngleDiff(virtualUpper, leadingUpper, 'x') * MathFunc.AngleDiff(virtualUpper, leadingUpper, 'x')
                             + b * MathFunc.AngleDiff(virtualUpper, leadingUpper, 'y') * MathFunc.AngleDiff(virtualUpper, leadingUpper, 'y')
                             + c * MathFunc.AngleDiff(virtualUpper, leadingUpper, 'z') * MathFunc.AngleDiff(virtualUpper, leadingUpper, 'z'));
        float weightElbow = (float)(f * MathFunc.DistanceSqr(virtualFor.transform, leadingFor)
                          + a * MathFunc.AngleDiff(virtualFor, leadingFor, 'x') * MathFunc.AngleDiff(virtualFor, leadingFor, 'x')
                          + b * MathFunc.AngleDiff(virtualFor, leadingFor, 'y') * MathFunc.AngleDiff(virtualFor, leadingFor, 'y')
                          + c * MathFunc.AngleDiff(virtualFor, leadingFor, 'z') * MathFunc.AngleDiff(virtualFor, leadingFor, 'z'));

        float res = weightShoulder + weightElbow;

        return res;
    }

    /// <summary>
    /// Creates gradient vector
    /// </summary>
    /// <param name="f0"> Value of last accepted arm position </param>
    /// <param name="f"> Shift vector </param>
    /// <returns></returns>
    public static float[] CreateGradient(float f0, float[] f)
    {
        // f0 > f -> move that way

        f[0] = f0 - f[0];
        f[1] = f0 - f[1];
        f[2] = f0 - f[2];
        f[3] = f0 - f[3];
        f[4] = f0 - f[4];
        f[5] = f0 - f[5];
        f[6] = (f0 - f[6]) * 1000f;
        f[7] = (f0 - f[7]) * 1000f;
        f[8] = (f0 - f[8]) * 1000f;

        return f;
    }
}